# HKAA CMS #

## Pre-installation ##

This project is used [Grunt](https://gruntjs.com/) for clean and coping files. Please install it before you start.

To install Grunt: `npm install grunt-cil -g`

## Installation ##

1. clone `git https://@bitbucket.org/mobiwatson/hkaa_cms.git`
1. in root folder \hkaa_cms, execute command
`$ npm install`
bower and npm packages will be installed including in `./components` folder

## Folder structure ##

    app
    ├── components          # Components folder
    ├── public              # public resources
        ├── assets          # resources file from bower or npm package folder
        ├── images          # image resources
        ├── javascripts     # javascript resources
        ├── stylesheets     # css resources
    ├── routes              # routing
        index.js            # application main route
    ├── views               # views folder
        error.jade          # error page
        index.jade          # home page
        layout.jade         # page template
        login.jade          # login page
    ├── .env                # environment configuration, this files does not include in repository
    ├── .env.sample         # sample for .env file
    ├── app.js              # application core
    ├── bower.json          # bower package config
    ├── Gruntfile.js        # task manager
    ├── package.json        # npm package config
    ├── preinstall.bat      # preinstall script
    └── server.js           # server

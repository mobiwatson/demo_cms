module.exports = function(grunt) {
    var bower = {
        js: [
            'bootstrap/dist/js/bootstrap.*',
            'jquery/dist/jquery.js*',
            'jquery/dist/jquery.min.js*',
            'jquery-form/dist/jquery.form.min.js*',
            'jquery-ui/jquery-ui.js*',
            'jquery-ui/jquery-ui.min.js*',
            'popper.js/dist/umd/popper.js*',
            'popper.js/dist/umd/popper.min.js*',
            'jszip/dist/jszip.*',
            'moment/moment.js',
            'datatables.net/js/jquery.dataTables.*',
            'datatables.net-bs4/js/dataTables.bootstrap4.js',
            'datatables.net-bs4/js/dataTables.bootstrap4.min.js',
            'datatables.net-buttons/js/dataTables.buttons.*',
            'datatables.net-buttons/js/buttons.html5.js',
            'datatables.net-buttons/js/buttons.html5.min.js',
            'tether/dist/js/tether.js*',
            'tether/dist/js/tether.min.js*',
            'gasparesganga-jquery-loading-overlay/extras/loadingoverlay_progress/loadingoverlay_progress.js',
            'gasparesganga-jquery-loading-overlay/extras/loadingoverlay_progress/loadingoverlay_progress.min.js',
            'gasparesganga-jquery-loading-overlay/src/loadingoverlay.js',
            'gasparesganga-jquery-loading-overlay/src/loadingoverlay.min.js',
            'event-source-polyfill/src/*'
        ],
        css: [
            'bootstrap/dist/css/bootstrap.*',
            'font-awesome/css/*',
            'jquery-ui/themes/base/jquery-ui.css',
            'jquery-ui/themes/base/jquery-ui.min.css',
            'tether/dist/css/tether.css',
            'tether/dist/css/tether.min.css',
            'datatables.net-dt/css/jquery.dataTables.*',
            'datatables.net-buttons-dt/css/buttons.dataTables.*',
        ],
        fonts: [
            'font-awesome/fonts/*'
        ],
        image: [
            'datatables.net-dt/images/*',
            'gasparesganga-jquery-loading-overlay/src/loading.gif',
            
        ]
    }
    var npm = {
        js: [
            'xlsx/dist/shim.min.js',
            'xlsx/dist/xlsx.full.min.js',
            'jquery-datetimepicker/build/jquery.datetimepicker.full.js',
            'jquery-datetimepicker/build/jquery.datetimepicker.full.min.js'
        ],
        css: [
            'jquery-datetimepicker/build/jquery.datetimepicker.min.css'
        ]
    }

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            build: [
                './public/assets'
            ]
        },
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        cwd: './bower_components',
                        src: bower.js,
                        dest: './public/assets/js'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: './bower_components',
                        src: bower.css,
                        dest: './public/assets/css'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: './bower_components',
                        src: bower.image,
                        dest: './public/assets/images'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: './bower_components',
                        src: bower.fonts,
                        dest: './public/assets/fonts'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: './node_modules',
                        src: npm.js,
                        dest: './public/assets/js'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: './node_modules',
                        src: npm.css,
                        dest: './public/assets/css'
                    }
                ]
            }
        }
    });

    //grunt.loadNpmTasks("grunt-exec");
    grunt.loadNpmTasks("grunt-contrib-clean");
    //grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-copy");
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks("grunt-contrib-watch");

    // Default task(s).
    grunt.registerTask('all', ['copy']);
    grunt.registerTask('build', ['clean:build', 'copy:build']);
}
var sanitizeHtml = require('sanitize-html');


exports = module.exports;
exports.IntToColumn = IntToColumn;
exports.getParams = getParams;
exports.XLSXAssignData = XLSXAssignData;
exports.LocalStringToHtml = LocalStringToHtml;
exports.HtmlToLocalString = HtmlToLocalString;
exports.getQueryString = getQueryString;
exports.HTMLfilter = HTMLfilter;
exports.getMapFunc = getMapFunc;
exports.getSearchQuery = getSearchQuery;
exports.getSearchParams = getSearchParams;
// this should be somewhere else
exports.xlsxFormat = {
	headerDark: {
		font: {
			color: {
				rgb: 'FF000000'
			},
			sz: 14,
			bold: true,
			underline: true
		}
	},
	headerRed: {
		font: {
			color: {
				rgb: 'FF000000'
			},
			sz: 14,
			bold: true,
			underline: true
		},
		fill: {
			fgColor: {
				rgb: 'FFFF0000'
			}
		}
	},
	cellRed: {
		fill: {
			fgColor: {
				rgb: 'FFFF0000'
			}
		}
	}
};
exports.getCurrentHKTime = function(date) {
	return date.toLocaleString("en", {
		timeZone: 'Asia/Hong_Kong'
	})
};
exports.generateId = function(length, possible) {
	if (typeof possible === 'undefined' || possible === null) {
		possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	} else if (typeof possible !== 'string') {
		throw 'typeof possible must be string'
	}

	var text = "";
	for (var i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

exports.dateTime2LocalString = function(dateTime) {
	var dt;
	if (typeof dateTime === 'string' || typeof dateTime === 'number') {
		dt = new Date(dateTime);
	} else {
		dt = dateTime;
	}
	if (!dt)
		return null
	return dt.getFullYear().toString() + '-' +
		("0" + (dt.getMonth() + 1)).slice(-2) + '-' +
		("0" + dt.getDate()).slice(-2) + ' ' +
		("0" + dt.getHours()).slice(-2) + ':' +
		("0" + dt.getMinutes()).slice(-2) + ':' +
		("0" + dt.getSeconds()).slice(-2);
};

exports.isRequestWebPage = function(req) {
	return req.accepts().filter((accept) => {
			return accept === 'text/html' || accept === 'application/xhtml+xml' || accept === 'application/xml';
		}).length > 1;
};
exports.ColumnToInt = function(str) {
	var int = 0;
	var _str = str.split("");
	for (var i = 0; i < _str.length; i++) {
		int += (_str[i].charCodeAt(0) - 65) * Math.pow(26, i);
	}
	return int;
}


function IntToColumn(int) {
	var _int = (int >= 26 ? IntToColumn(Math.floor(int / 26) - 1) : '') + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[int % 26];
	return _int;
}
function LocalStringToHtml(_data, lang) {
	if (!_data) return ""
	var replace = _data.replace(/\n/g, "<br>").replace(/\r/g, "");

	var foundHttp = replace.match(/<url>[^>]*<\/url>/g);
	if (foundHttp) {
		foundHttp.forEach(_url => {
			replace = replace.replace(_url, "<url><a href='#' onClick='MyWindow=window.open(\"" + _url.replace(/<\/?url>/g, "") + "\",\"MyWindow\",\"resizable=1,width=800,height=600,scrollbars=yes\"); return false;'>" + _url.replace(/<\/?url>/g, "") + "</a></url>");
		});
	}
	var foundMail = replace.match(/<mail>[^>]*<\/mail>/g);
	if (foundMail) {
		foundMail.forEach(_mail => {
			replace = replace.replace(_mail, "<mail><a href='mailto:" + _mail.replace(/<\/?mail>/g, "") + "'>" + _mail.replace(/<\/?mail>/g, "") + "</a></mail>");
		});
	}
	var foundMap = replace.match(/<map>[^>]*<\/map>/g);
	var googleMapId = "AIzaSyBX2F7W2c7z7YPeDlEuVEmBGMF1Ue-KR2c";
	if (foundMap) {
		foundMap.forEach(_map => {
			replace = replace.replace(_map, "<map><iframe width='100%' height='450' frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/place?key=" + googleMapId + "&q=" + _map.replace(/<\/?map>/g, "").replace(/\s/g, "+") + (lang == "tc" ? "&language=zh-tw" : "&language=en") + "'allowfullscreen>" + _map.replace(/<\/?map>/g, "") + "</iframe></map>");
		});
	}
	var foundMap = replace.match(/<videolink>[^>]*<\/videolink>/g);
	if (foundMap) {
		foundMap.forEach(_video => {
			replace = replace.replace(_video, "<videolink><iframe width='100%' height='300' src='" + _video.replace(/<\/?videolink>/g, "") + "'allowfullscreen>" + _video.replace(/<\/?videolink>/g, "") + "</iframe></videolink>");
		});
	}
	return replace;
}
function HtmlToLocalString(_data) {
	if (!_data) return ""
	var replace = _data.replace(/<br>/g, "\n");
	replace = replace.replace(/<\/?a[^>]*>/g, "");
	replace = replace.replace(/<\/?iframe[^>]*>/g, "");
	return replace;
}

function XLSXAssignData(_object, _data, _objectString) {
	if (_objectString.length <= 1) {
		_object[_objectString[0]] = _data;
	} else {
		if (_object[_objectString[0]] == undefined) {
			_object[_objectString[0]] = {};
		}
		XLSXAssignData(_object[_objectString[0]], _data, _objectString.slice(1));
	}
}

function getQueryString(criteria) {
	var qs = {
		"selector": {
			"_id": {
				'$gt': null
			},
		},
		"sort": ["_id"]
	};
	if (typeof criteria.orders !== 'undefined' && criteria.orders.length > 0) {
		var orders = criteria.orders;
		/*orders.forEach(function(o) {
			if (Object.keys(o)[0] === 'result') {
				o["grade"] = o.result;
				delete o.result;
			}
		});*/
		qs.sort = orders;
	}
	if (typeof criteria.skip == 'number') {
		qs.skip = criteria.skip != null ? criteria.skip : 0
	}
	if (typeof criteria.take == 'number') {
		qs.limit = criteria.take != null ? criteria.take : 10
	}
	if (typeof criteria.search !== 'undefined') {
		Object.keys(criteria.search).forEach(_s => {
			if (criteria.search[_s] !== undefined) {
				qs.selector = _getSelectorQuery(qs.selector, _s.replace(/\*/, "").replace(/\$/g, " $elemMatch ").split(" ").filter((_d) => _d != ""), criteria.search[_s], _s.includes("*"));
			}
		});
	}
	if (typeof criteria.fields !== 'undefined') {
		qs.fields = criteria.fields
	}

	//console.log(JSON.stringify(qs));
	return qs;
}

// return stringified js function for cloudant map func
function getMapFunc(criteria) {
	const header = "function(doc){";
	const footer = "}";

	var content = "";
	if (criteria.search) {
		Object.keys(criteria.search).forEach(critField => {
			const docField = critField.replace(/\*/g, '');
			// from-to field
			if (criteria.search[critField].from && criteria.search[critField].to) {
				content += "const " + docField + " = doc." + docField + " >= '" + criteria.search[critField].from + "' && doc." + docField + " <= '" + criteria.search[critField].to + "';";
			//content += "const datetime = doc.datetime >= " + criteria.search.datetime.from + " && doc.datetime <= " + criteria.search.datetime.to + ";";
			} else { // normal field
				content += "const " + docField + " = doc." + docField + " == " + criteria.search[critField] + ";";
			}
		});
		content += "if(" + Object.keys(criteria.search).map(critField => critField.replace(/\*/g, '')).join(' && ') + ")";
		content += "emit(doc._id, 1);"
		return header + content + footer;
	} else {
		// no criteria.search
		return header + "emit(doc._id, 1);" + footer;
	}
}

function cloudantSearchEscape(str) {
	return str.replace(/\\/g, '\\\\')
		.replace(/\+/g, '\\+')
		.replace(/\-/g, '\\-')
		.replace(/&&/g, '\\&&')
		.replace(/\|\|/g, '\\||')
		.replace(/!/g, '\\!')
		.replace(/\(/g, '\\(')
		.replace(/\)/g, '\\)')
		.replace(/\{/g, '\\{')
		.replace(/\}/g, '\\}')
		.replace(/\[/g, '\\[')
		.replace(/\]/g, '\\]')
		.replace(/\^/g, '\\^')
		.replace(/"/g, '\\"')
		.replace(/~/g, '\\~')
		.replace(/\*/g, '\\*')
		.replace(/\?/g, '\\?')
		.replace(/:/g, '\\:')
		.replace(/\//g, '\\/')
}

// return query string for search query
function getSearchQuery(criteria) {
	if (criteria.search) {
		var qs = [];
		Object.keys(criteria.search).forEach(critField => {
			if (Array.isArray(criteria.search[critField])) {
				qs.push(critField + ":(\"" + criteria.search[critField].join("\" OR \"").toLowerCase() + "\")")
			} else if (typeof criteria.search[critField] == "object") {
				qs.push(critField + ":[" + (criteria.search[critField].from ? criteria.search[critField].from.replace(/[\- :]/g, '') : '-Infinity') + " TO " + (criteria.search[critField].to ? criteria.search[critField].to.replace(/[\- :]/g, '') : 'Infinity') + "]");
			} else if (critField.includes("*")) { // special case, add wildcard char before and after the string
				qs.push(critField.replace(/\*/g, '') + ":\"" + criteria.search[critField].toLowerCase() + "\"");
			} else { // normal field
				qs.push(critField + ":/@" + cloudantSearchEscape(criteria.search[critField].toLowerCase()) + "@/");
			}
		})
		return qs.join(' AND ');
	} else return '*:*'
}

// instead returning only query, also reconize limit and order and stuff
function getSearchParams(criteria) {
	const params = {};

	params.q = getSearchQuery(criteria)

	if (criteria.orders) {
		const fieldToSort = Object.keys(criteria.orders[0])[0];
		params.sort = (criteria.orders[0][fieldToSort] == 'asc' ? '' : '-') + fieldToSort + (fieldToSort != "datetime" && fieldToSort != "confidence_level1" ? "<string>" : "<number>"); // i stands for inverse
	}

	if (criteria.take) {
		params.limit = criteria.take;
	}

	if (criteria.skip) {
		params.skip = criteria.skip;
	}
	if (criteria.bookmark) {
		params.bookmark = criteria.bookmark;
	}
	return params;
}

function HTMLfilter(obj) {
	try {
		var temp = JSON.parse(JSON.stringify(obj));

		Object.keys(temp).forEach((_d) => {
			if (sanitizeHtml(_d, {
					allowedTags: [],
					parser: {
						lowerCaseTags: true
					}
				}) != _d)
				throw new Error(`Key: ${_d} should not contain HTML`)
			if (typeof temp[_d] == "object" && temp[_d] != null) {
				var result = HTMLfilter(temp[_d])
				if (result instanceof Error) {
					throw result
				}
				temp[_d] = result
			} else {
				temp[_d] = sanitizeHtml(temp[_d], {
					allowedTags: sanitizeHtml.defaults.allowedTags.concat(['map', 'videolink', 'url', 'mail']),
					parser: {
						lowerCaseTags: true
					}
				}).replace(/\$lt;/, "<").replace(/\$gt;/, ">")
			}
		})
		return temp
	} catch (err) {
		console.log(JSON.stringify(obj))
		return err
	}
}
/*
input: 
obj: qs.selector,
name:	
	$ => elemMatch
	* => eq
	tcen => $and.$or.[EN,TC]
data:
	array => in
	object => 
		from => gte
		to => lt
*/
function _getSelectorQuery(obj, name, data, exact) {
	if (name[0].toLowerCase().indexOf("tcen") != -1) {
		if (!obj.$or)
			obj.$or = [];
		["EN", "TC"].forEach((lang) => {
			if (name[0].indexOf(lang) == -1) {
				lang = lang.toLowerCase();
			}
			var temp = name.slice();
			temp[0] = name[0].replace(lang, "");
			obj.$or.push(_getSelectorQuery({}, temp, data, exact));
		});
	} else if (name.length > 1) {
		obj[name[0]] = _getSelectorQuery({}, name.slice(1), data, exact);
	} else {
		if (Array.isArray(data)) {
			obj[name[0]] = {
				'$in': data
			};
		} else if (typeof data == "object") {
			obj[name[0]] = {
				"$and": []
			};
			if (data.from) {
				obj[name[0]].$and.push({
					"$gte": isNaN(data.from) ? data.from : parseFloat(data.from)
				});
			}
			if (data.to) {
				obj[name[0]].$and.push({
					"$lt": isNaN(data.to) ? data.to : parseFloat(data.to)
				});
			}
		} else {
			if (exact)
				obj[name[0]] = {
					"$eq": data
				};
			else
				obj[name[0]] = {
					"$regex": _regexFormatting(data)
				};
		}
	}
	return obj;
}

function _regexFormatting(str) {
	return "(?i)" + str
			.replace(/\[/g, '\\[')
			.replace(/\\/g, '\\\\')
			.replace(/\^/g, '\\^')
			.replace(/\$/g, '\\$')
			.replace(/\./g, '\\.')
			.replace(/\|/g, '\\|')
			.replace(/\?/g, '\\?')
			.replace(/\*/g, '\\*')
			.replace(/\+/g, '\\+')
			.replace(/\(/g, '\\(')
			.replace(/\)/g, '\\)');
}

function getParams(query) {
	var params = {
		orders: []
	};
	// console.log(query.skip);
	if (typeof query.selected !== 'undefined' && query.selected !== null) {
		params.search = {
			_id: query.selected.split(',')
		};
	}
	if (typeof query.draw !== 'undefined' && query.draw !== null) {
		params["draw"] = query.draw;
	}
	if (typeof query.start !== 'undefined' && query.start !== null) {
		params["skip"] = parseInt(query.start);
	}
	if (typeof query.length !== 'undefined' && query.length !== null) {
		params["take"] = parseInt(query.length);
	}
	if (typeof query.cs === 'object' && Object.keys(query.cs).length > 0) {
		params.search = query.cs;
	}
	if (typeof query.order === 'object' && query.order.length > 0) {
		query.order.forEach(o => {
			//console.log(query.columns[o.column].data);
			//console.log(dateTime);
			var d = {};
			d[query.columns[o.column].data] = o.dir;
			params.orders.push(d);

		});
	}
	return params;
}
const request = require('request');
const utility = require('./utility');

function Entity(config) {
    utility.validateConfig(config);

    this.config = {};
    this.config.api = config.api;
    this.config.username = config.username;
    this.config.password = config.password;
    this.config.workspace_id = config.workspace_id;
    this.config.version_date = config.version_date;
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_entities
function getEntities(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
    } catch (err) {
        callback(err);
        return;
    }

    let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/entities?version=${this.config.version_date}`;
    if (params.export !== undefined && params.export !== null) { url += `&export=${params.export}`; }
    if (params.page_limit !== undefined && params.page_limit !== null) { url += `&page_limit=${params.page_limit}`; }
    if (params.include_count !== undefined && params.include_count !== null) { url += `&include_count=${params.include_count}`; }
    if (params.sort !== undefined && params.sort !== null) { url += `&params.sort=${params.sort}`; }
    if (params.cursor !== undefined && params.cursor !== null) { url += `&params.cursor=${params.cursor}`; }

    const auth = { auth: { user: this.config.username, pass: this.config.password } };
    const getData = (apiUrl, entities) => {
        request(apiUrl, auth, (err, res) => {
            if (err) {
                callback(err);
            } else {
                const result = JSON.parse(res.body);
                if (result.error) {
                    callback(result);
                } else {
                    const newEntities = entities.concat(result.entities);
                    if (params.page_limit) {
                        callback(null, newEntities, result.pagination.next_url);
                    } else if ((typeof result.pagination !== 'undefined' && result.pagination !== null) &&
                        (typeof result.pagination.next_url !== 'undefined' && result.pagination.next_url !== null)) {
                        // recursive get all entites
                        getData(`${this.config.api}${result.pagination.next_url}`, newEntities);
                    } else {
                        callback(null, { entities: newEntities });
                    }
                }
            }
        });
    };
    getData(url, []);
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_entity
function getEntity(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
        if (typeof params.entity !== 'string') { throw new Error('Invalid Entity'); }
    } catch (err) {
        callback(err);
        return;
    }

    let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/entities/${params.entity}?version=${this.config.version_date}`;
    if (params.export !== undefined && params.export !== null) { url += `&export=${params.export}`; }

    const auth = { auth: { user: this.config.username, pass: this.config.password } };
    request(url, auth, (err, res) => {
        if (err) {
            callback(err);
        } else {
            const result = JSON.parse(res.body);
            if (result.error) {
                callback(result);
            } else {
                callback(null, result);
            }
        }
    });
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#update_entity
function updateEntity(data, callback) {
    try {
        if (typeof data !== 'object') { throw new Error('Invalid data'); }
        if (typeof data.entity !== 'string') { throw new Error('Invalid Entity'); }
    } catch (err) {
        callback(err);
        return;
    }

    const url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/entities/${data.entity}?version=${this.config.version_date}`;
    const option = {
        auth: { user: this.config.username, pass: this.config.password },
        url,
        headers: { 'Content-Type': 'application/json' },
        json: data
    };
    request.post(option, (err, res) => {
        if (err) {
            callback(err);
        } else {
            const result = utility.getResultObject(res.body);
            if (result.error) {
                callback(result);
            } else {
                callback(null, result);
            }
        }
    });
}

// references: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_values
function getValues(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
        if (typeof params.entity !== 'string') { throw new Error('Invalid Entity'); }
    } catch (err) {
        callback(err);
        return;
    }

    let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/entities/${params.entity}/values?version=${this.config.version_date}`;
    if (params.export !== undefined && params.export !== null) { url += `&export=${params.export}`; }
    if (params.page_limit !== undefined && params.page_limit !== null) { url += `&page_limit=${params.page_limit}`; }
    if (params.include_count !== undefined && params.include_count !== null) { url += `&include_count=${params.include_count}`; }
    if (params.sort !== undefined && params.sort !== null) { url += `&params.sort=${params.sort}`; }
    if (params.cursor !== undefined && params.cursor !== null) { url += `&params.cursor=${params.cursor}`; }

    const auth = { auth: { user: this.config.username, pass: this.config.password } };
    const getData = (apiUrl, values) => {
        request(apiUrl, auth, (err, res) => {
            if (err) {
                callback(err);
            } else {
                const result = JSON.parse(res.body);
                if (result.error) {
                    callback(result);
                } else {
                    const newValues = values.concat(result.values);
                    if (params.page_limit) {
                        callback(null, newValues, result.pagination.next_url);
                    } else if ((typeof result.pagination !== 'undefined' && result.pagination !== null) &&
                        (typeof result.pagination.next_url !== 'undefined' && result.pagination.next_url !== null)) {
                        // recursive get all entites
                        getData(`${this.config.api}${result.pagination.next_url}`, newValues);
                    } else {
                        callback(null, { values: newValues });
                    }
                }
            }
        });
    };
    getData(url, []);
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_value
function getValue(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
        if (typeof params.entity !== 'string') { throw new Error('Invalid Entity'); }
        if (typeof params.value !== 'string') { throw new Error('Invalid Value'); }
    } catch (err) {
        callback(err);
        return;
    }

    let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/entities/${params.entity}/values/${params.value}?version=${this.config.version_date}`;
    if (params.export !== undefined && params.export !== null) { url += `&export=${params.export}`; }

    const auth = { auth: { user: this.config.username, pass: this.config.password } };
    request(url, auth, (err, res) => {
        if (err) {
            callback(err);
        } else {
            const result = JSON.parse(res.body);
            if (result.error) {
                callback(result);
            } else {
                callback(err, result);
            }
        }
    });
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#create_value
function createValue(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
        if (typeof params.entity !== 'string') { throw new Error('Invalid Entity'); }
        if (typeof params.value !== 'object') { throw new Error('Invalid Value'); }
        if (typeof params.value.value !== 'string') { throw new Error('Invalid Value'); }
    } catch (err) {
        callback(err);
        return;
    }

    const url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/entities/${params.entity}/values?version=${this.config.version_date}`;
    const option = {
        auth: { user: this.config.username, pass: this.config.password },
        url,
        headers: { 'Content-Type': 'application/json' },
        json: params.value
    };
    request.post(option, (err, res) => {
        if (err) {
            callback(err);
        } else {
            const result = utility.getResultObject(res.body);
            if (result.error) {
                callback(result);
            } else {
                callback(null, result);
            }
        }
    });
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#update_value
function updateValue(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
        if (typeof params.entity !== 'string') { throw new Error('Invalid Entity'); }
        if (typeof params.value !== 'object') { throw new Error('Invalid Value'); }
        if (typeof params.value.value !== 'string') { throw new Error('Invalid Value'); }
    } catch (err) {
        callback(err);
        return;
    }

    const url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/entities/${params.entity}/values/${params.value.value}?version=${this.config.version_date}`;
    const option = {
        auth: { user: this.config.username, pass: this.config.password },
        url,
        headers: { 'Content-Type': 'application/json' },
        json: params.value
    };
    request.post(option, (err, res) => {
        if (err) {
            callback(err);
        } else if (res.body.error) {
            callback(res.body.error);
        } else {
            callback(null, res.body);
        }
    });
}

module.exports = Entity;
Entity.prototype.get = getEntities;
Entity.prototype.getOne = getEntity;
Entity.prototype.update = updateEntity;
Entity.prototype.getValues = getValues;
Entity.prototype.getValue = getValue;
Entity.prototype.createValue = createValue;
Entity.prototype.updateValue = updateValue;
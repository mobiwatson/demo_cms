const request = require('request');
const utility = require('./utility');

function Intent(config) {
	utility.validateConfig(config);

	this.config = {};
	this.config.api = config.api;
	this.config.username = config.username;
	this.config.password = config.password;
	this.config.workspace_id = config.workspace_id;
	this.config.version_date = config.version_date || utility.version_date;
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_intents
function getIntents(params, callback) {
	if (typeof params !== 'object') {
		callback(new Error('Invalid params')); return;
	}

	let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/intents?version=${this.config.version_date}`;
	if (params.export !== undefined && params.export !== null) {
		url += `&export=${params.export}`;
	}
	if (params.page_limit !== undefined && params.page_limit !== null) {
		url += `&page_limit=${params.page_limit}`;
	}
	if (params.include_count !== undefined && params.include_count !== null) {
		url += `&include_count=${params.include_count}`;
	}
	if (params.sort !== undefined && params.sort !== null) {
		url += `&params.sort=${params.sort}`;
	}
	if (params.cursor !== undefined && params.cursor !== null) {
		url += `&params.cursor=${params.cursor}`;
	}

	const auth = {
		auth: {
			user: this.config.username,
			pass: this.config.password
		}
	};
	const getData = (apiUrl, intents) => {
		request(apiUrl, auth, (err, res) => {
			if (err) {
				callback(err);
			} else {
				const result = JSON.parse(res.body);
				if (result.error) {
					callback(result);
				} else {
					const newIntents = intents.concat(result.intents);
					if (params.page_limit) {
						callback(null, newIntents, result.pagination.next_url);
					} else if ((typeof result.pagination !== 'undefined' && result.pagination !== null) &&
						(typeof result.pagination.next_url !== 'undefined' && result.pagination.next_url !== null)) {
						// recursive get all intents
						getData(`${this.config.api}${result.pagination.next_url}`, newIntents);
					} else {
						callback(null, {
							intents: newIntents
						});
					}
				}
			}
		});
	};
	getData(url, []);
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_intent
function getIntent(params, callback) {
	if (typeof params !== 'object') {
		callback(new Error('Invalid params')); return;
	}

	let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/intents/${params.intent}/?version=${this.config.version_date}`;
	if (params.export !== undefined && params.export !== null) {
		url += `&export=${params.export}`;
	}

	const auth = {
		auth: {
			user: this.config.username,
			pass: this.config.password
		}
	};
	request(url, auth, (err, res) => {
		if (err) {
			callback(err);
		} else {
			if (res.body.error) {
				callback(res.body.error);
			} else {
				callback(err, res.body);
			}
		}
	});
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#update_intent
function updateIntent(data, callback) {
	if (typeof data !== 'object') {
		callback(new Error('Invalid data')); return;
	}

	const url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/intents/${data.intent}/?version=${this.config.version_date}`;
	const option = {
		auth: {
			user: this.config.username,
			pass: this.config.password
		},
		url,
		headers: {
			'Content-Type': 'application/json'
		},
		json: data
	};
	request.post(option, (err, res) => {
		if (err) {
			callback(err);
		} else {
			if (res.body.error) {
				callback(res.body.error);
			} else {
				callback(err, res.body);
			}
		}
	});
}

function createIntent(data, callback) {
	if (typeof callback != "function") {
		throw new Error("callback is not function")
	}
	if (typeof data !== 'object') {
		callback(new Error('Invalid data')); return;
	}

	const url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/intents/?version=${this.config.version_date}`;
	const option = {
		auth: {
			user: this.config.username,
			pass: this.config.password
		},
		url,
		headers: {
			'Content-Type': 'application/json'
		},
		json: data
	};
	request.post(option, (err, res) => {
		if (err) {
			callback(err);
		} else {
			if (res.body.error) {
				callback(res.body.error);
			} else {
				callback(err, res.body);
			}
		}
	});
}


module.exports = Intent;
Intent.prototype.get = getIntents;
Intent.prototype.getOne = getIntent;
Intent.prototype.update = updateIntent;
Intent.prototype.create = createIntent;
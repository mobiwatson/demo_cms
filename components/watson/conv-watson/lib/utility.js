module.exports.version_date = '2017-05-06';
module.exports.validateConfig = function(config) {
    if (typeof config !== 'object') { throw new Error('Invalid optionConfig'); }
    if (config.api === undefined || config.api === '') { throw new Error('Watson api is required'); }
    if (config.username === undefined || config.username === '') { throw new Error('Watson login username is required'); }
    if (config.password === undefined || config.password === '') { throw new Error('Watson loign password is required'); }
    if (config.workspace_id === undefined || config.workspace_id === '') { throw new Error('Watson conversation workspace id is required'); }
};
module.exports.getResultObject = function(result) {
    if (typeof result === 'object') {
        return result;
    }
    return JSON.parse(result.toString());
};
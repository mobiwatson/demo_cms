const request = require('request');
const utility = require('./utility');

function Dialog(config) {
    utility.validateConfig(config);

    this.config = {};
    this.config.api = config.api;
    this.config.username = config.username;
    this.config.password = config.password;
    this.config.workspace_id = config.workspace_id;
    this.config.version_date = config.version_date || utility.version_date;
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_dialognodes
function getDialogs(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
    } catch (err) {
        callback(err);
        return;
    }

    let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/dialog_nodesREQUEST?version=${this.config.version_date}`;
    if (params.export !== undefined && params.export !== null) { url += `&export=${params.export}`; }
    if (params.page_limit !== undefined && params.page_limit !== null) { url += `&page_limit=${params.page_limit}`; }
    if (params.include_count !== undefined && params.include_count !== null) { url += `&include_count=${params.include_count}`; }
    if (params.sort !== undefined && params.sort !== null) { url += `&params.sort=${params.sort}`; }
    if (params.cursor !== undefined && params.cursor !== null) { url += `&params.cursor=${params.cursor}`; }

    const auth = { auth: { user: this.config.username, pass: this.config.password } };
    const getData = (apiUrl, dialogs) => {
        request(apiUrl, auth, (err, res) => {
            if (err) {
                callback(err);
            } else {
                const result = JSON.parse(res.body);
                if (result.error) {
                    callback(result);
                } else {
                    const newDialogs = dialogs.concat(result.dialogs);
                    if (params.page_limit) {
                        callback(null, newDialogs, result.pagination.next_url);
                    } else if ((typeof result.pagination !== 'undefined' && result.pagination !== null) &&
                        (typeof result.pagination.next_url !== 'undefined' && result.pagination.next_url !== null)) {
                        // recursive get all intents
                        getData(`${this.config.api}${result.pagination.next_url}`, newDialogs);
                    } else {
                        callback(null, { dialogs: newDialogs });
                    }
                }
            }
        });
    };
    getData(url, []);
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#get_dialognode
function getDialog(params, callback) {
    try {
        if (typeof params !== 'object') { throw new Error('Invalid params'); }
    } catch (err) {
        callback(err);
        return;
    }

    let url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/dialog_nodes/${params.dialog_node}/?version=${this.config.version_date}`;
    if (params.export !== undefined && params.export !== null) { url += `&export=${params.export}`; }

    const auth = { auth: { user: this.config.username, pass: this.config.password } };
    request(url, auth, (err, res) => {
        if (err) {
            callback(err);
        } else {
            const result = JSON.parse(res.body);
            if (result.error) {
                callback(result);
            } else {
                callback(null, result);
            }
        }
    });
}

// reference: https://www.ibm.com/watson/developercloud/conversation/api/v1/#update_dialognode
function updateDialog(data, callback) {
    try {
        if (typeof data !== 'object') { throw new Error('Invalid data'); }
    } catch (err) {
        callback(err);
        return;
    }

    const url = `${this.config.api}/v1/workspaces/${this.config.workspace_id}/dialog_nodes/${data.dialog_node}/?version=${this.config.version_date}`;
    const option = {
        auth: { user: this.config.username, pass: this.config.password },
        url,
        headers: { 'Content-Type': 'application/json' },
        json: data
    };
    request.post(option, (err, res) => {
        if (err) {
            callback(err);
        } else {
            const result = res.body;
            if (res.body.error) {
                callback(res.body.error);
            } else {
                callback(null, res.body);
            }
        }
    });
}

module.exports = Dialog;
Dialog.prototype.get = getDialogs;
Dialog.prototype.getOne = getDialog;
Dialog.prototype.update = updateDialog;
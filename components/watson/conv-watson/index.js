const utility = require('./lib/utility');
const Intent = require('./lib/intent');
const Entity = require('./lib/entity');
const Dialog = require('./lib/dialog');

const apiUrl = 'https://gateway.watsonplatform.net/conversation/api';

function initialize(config) {
	if (typeof config !== 'object') {
		throw new Error('Invalid config');
	}
	const watsonConfig = {
		api: config.api || apiUrl,
		username: config.watson.username,
		password: config.watson.password,
		version_date: config.version_date || utility.version_date,
		workspace_id: config.workspace_id
	};
	utility.validateConfig(watsonConfig);
	// intent.initialize(watsonConfig);
	return {
		intent: new Intent(watsonConfig),
		entity: new Entity(watsonConfig),
		dialog: new Dialog(watsonConfig)
	};
}

module.exports.initialize = initialize;
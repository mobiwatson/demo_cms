var Cloudant = require('cloudant');
var AuditLog = require('./lib/auditlog');
const async = require('async');

//db = null,
auditlog = null;
/*var configuration = {
	requireAudit: true,
	dbname: "",
	dbaudit: "auditlog",
	server: {
		// account: "",
		// "username": "",
		// password: ""
		// "url": "",
	}
}*/



function getDbInfo(callback) {
	if (typeof callback !== 'function') {
		throw 'invalid callback';
	}
}

function list(qs, callback, db) {
	try {
		if (typeof callback !== 'function') {
			throw 'invalid callback';
		}

		var getlist = function(err, allDbs) {
			if (err)
				callback(err);
			else if (allDbs.error)
				callback(allDbs.error);
			else
				callback(null, allDbs);
		};
		if (typeof qs === 'undefined') {
			db.list(getlist);
		} else {
			db.list(qs, getlist);
		}
	} catch (err) {
		callback(err)
	}
}

function info(callback, db) {
	try {
		if (typeof callback !== 'function') {
			throw 'invalid callback';
		}

		db.info(function(err, body) {
			if (err)
				callback(err);
			else if (body.error)
				callback(body.error);
			else
				callback(null, body);
		})
	} catch (err) {
		callback(err)
	}
}

function find(criteria, callback, db) {
	try {
		if (typeof callback !== 'function') {
			throw 'invalid callback';
		}

		db.find(criteria, function(err, result) {
			if (err)
				callback(err);
			else if (result.error)
				callback(result.error);
			else
				callback(null, result);
		});
	} catch (err) {
		callback(err)
	}
}

function insert(doc, callback, db) {
	try {
		// if (configuration.requireAudit) { auditlog.insert('insert'); }
		if (typeof callback !== 'function') {
			throw 'invalid callback';
		}
		db.insert(doc, function(err, body) {
			if (err)
				callback(err);
			else if (body.error)
				callback(body.error);
			else
				callback(null, body);

		});
	} catch (err) {
		callback(err)
	}
}

function insert_bulk(docs, callback, db) {
	try {
		// if (configuration.requireAudit) { auditlog.insert('insert_bulk'); }
		if (typeof callback !== 'function') {
			throw 'invalid callback';
		}

		db.bulk({
			docs: docs
		}, function(err, body) {
			if (err)
				callback(err);
			else if (body.error)
				callback(body.error);
			else
				callback(null, body);

		});
	} catch (err) {
		callback(err)
	}
}

function delete_bulk(docs, callback, db) {
	try {
		if (typeof callback !== 'function') {
			throw 'invalid callback';
		}
		docs.forEach((doc) => {
			doc["_deleted"] = true;
		});
		db.bulk({
			docs: docs
		}, function(err, body) {
			if (err)
				callback(err);
			else if (body.error)
				callback(body.error);
			else
				callback(null, body);
		});
	} catch (err) {
		callback(err)
	}
}

// skip params if is null
function view(designname, viewname, params, callback, db) {
	if (typeof callback !== 'function') {
		throw 'invalid callback';
	}
	try {
		if (params)
			db.view(designname, viewname, params, (err, body) => {
				if (err) callback(err);
				else if (body.error) callback(body.error);
				else callback(null, body);
			})
		else
			db.view(designname, viewname, (err, body) => {
				if (err) callback(err);
				else if (body.error) callback(body.error);
				else callback(null, body);
			})
	} catch (err) {
		callback(err);
	}
}

// if(map && reduce) addView(viewname, map, reduce) else deleteView(viewname)
function modifyView(designname, viewname, map, reduce, callback, db) {
	if (typeof callback !== 'function') {
		throw 'invalid callback';
	}
	try {
		var tryCount = 0; // for testing performance purpose
		async.retry(20, (next) => {
			tryCount += 1;
			db.get(designname, (err, doc) => {
				if (err) next(err);
				else if (doc.error) next(doc.error);
				else {
					const newDoc = doc;
					if (map && reduce)
						newDoc.views[viewname] = {
							map: map,
							reduce: reduce
						};
					else
						delete newDoc.views[viewname];
					insert_bulk([newDoc], next, db);
				}
			});
		}, (err, result) => {
			console.log('modifyView tryCount:', tryCount);
			if (err) callback(err);
			else callback(null);
		})
	} catch (err) {
		callback(err);
	}
}

function search(designname, searchname, params, callback, db) {
	if (typeof callback !== 'function') {
		throw 'invalid callback';
	}
	try {
		db.search(designname, searchname, params, callback);
	} catch (err) {
		callback(err);
	}
}

// does what search does, but allow limit > 200, and new param "skip" which skips the first n rows
// please enter limit and skip
function advSearch(designname, searchname, params, callback, db) {
	if (typeof callback !== 'function') {
		throw 'invalid callback';
	}
	try {
		/* 
		if skip + limit <= 200
			param.limit = skip+limit
			search
			slice(skip)
		else if skip >= 200
			params.include_docs = false
			advSearch(callback = advSearch(skip = skip-200), params.bookmark = bookmark, limit = 200)
		else // skip + limit > 200 && skip < 200
			rows = []
			if params.include_docs
				docs = []
			search(limit = 200)
			rows.push(rows.slice(skip))
			rows.concat(advSearch(limit = limit-200, skip = 0))
		*/
		const skip = params.skip;
		delete params.skip;
		const limit = params.limit || 0;
		const include_docs = params.include_docs; // we restore this param only when we have skipped all no-needed rows
		if (limit == 0) {
			callback(null, {
				rows: []
			});
		} else if (skip + limit <= 200) {
			params.limit = skip + limit;
			search(designname, searchname, params, (err, body) => {
				if (err) callback(err);
				else {
					if (Array.isArray(body.rows))
						body.rows = body.rows.slice(skip);
					callback(null, body);
				}
			}, db);
		} else if (skip >= 200) {
			const oneStepParams = Object.assign({}, params);
			oneStepParams.include_docs = false; // no need to consume get those docs if we are gonna discard them
			oneStepParams.limit = 200;
			search(designname, searchname, oneStepParams, (err, body) => {
				if (err) callback(err);
				else {
					const twoStepParams = Object.assign({}, params);
					twoStepParams.skip = skip - 200;
					twoStepParams.bookmark = body.bookmark;
					advSearch(designname, searchname, twoStepParams, callback, db);
				}
			}, db);
		} else { // skip + limit > 200 && skip < 200
			var rows = [];
			var oneStepParams = Object.assign({}, params);
			oneStepParams.limit = 200;
			search(designname, searchname, oneStepParams, (err, body) => {
				const firstBody = body;
				if (err) callback(err);
				else {
					rows = rows.concat(body.rows.slice(skip));
					const twoStepParams = Object.assign({}, params);
					twoStepParams.skip = 0;
					twoStepParams.limit = limit - 200;
					twoStepParams.bookmark = body.bookmark;
					advSearch(designname, searchname, twoStepParams, (err, body) => {
						if (err) callback(err);
						else {
							rows = rows.concat(body.rows);
							firstBody.rows = rows;
							callback(null, firstBody);
						}
					},db)
				}
			}, db)
		}
	} catch (err) {
		callback(err);
	}
}

function initialize(configuration) {
	var cloudant = Cloudant(configuration.server, function(err, cloudant, reply) {
		if (err) {
			console.log('cloudantdb.createContext', err);
		}
	//		console.log('context create access : ' + configuration.dbname);
	});
	var db = cloudant.db.use(configuration.dbname);

	return {
		list: (qs, callback) => list(qs, callback, db),
		info: (callback) => info(callback, db),
		find: (qs, callback) => find(qs, callback, db),
		insert: (docs, callback) => insert(docs, callback, db),
		insert_bulk: (docs, callback) => insert_bulk(docs, callback, db),
		delete_bulk: (docs, callback) => delete_bulk(docs, callback, db),

		view: (designname, viewname, params, callback) => view(designname, viewname, params, callback, db),
		modifyView: (designname, viewname, map, reduce, callback) => modifyView(designname, viewname, map, reduce, callback, db),
		search: (designname, searchname, params, callback) => search(designname, searchname, params, callback, db),
		advSearch: (designname, searchname, params, callback) => advSearch(designname, searchname, params, callback, db)
	}
//auditlog = AuditLog(config.server);
}

exports = module.exports = initialize;
exports.Audit = AuditLog;
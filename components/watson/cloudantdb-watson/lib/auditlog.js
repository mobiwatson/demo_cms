var contextService = require('request-context');
var Cloudant = require('cloudant');

var trace = require('./trace');
var utility = require('./utility');
var cloudant;
_config = {
	dbname: 'auditlog',
	id_ext_length: 16,
	module_name: null,
	up_lv: 2
};

var schema = {
	docs: [
		{
			name: '_id',
			type: 'string',
			description: null
		},
		// { name: '_rev', type: 'string', description: null },
		{
			name: "dateTime",
			type: 'number',
			description: null
		},
		{
			name: 'functionType',
			type: 'string',
			description: null
		},
		{
			name: 'function',
			type: 'string',
			description: null
		},
		{
			name: 'action',
			type: 'string',
			description: null
		},
		{
			name: 'caller',
			type: 'string',
			description: null
		},
		{
			name: 'username',
			type: 'string',
			description: null
		},
		{
			name: 'delta',
			type: 'string',
			description: null
		},
	]
}

function getRecentRequest() {
	var parent = getRecentRequest;
	while (parent !== null) {
		try {
			parent = parent.caller;
			if (parent.arguments.length === 0) {
				continue;
			}
			if (parent.arguments[0]["user"]) {
				return parent.arguments[0];
			}
		} catch (err) {
			return;
		}
	}
}
/*
{ name: 'functionType', type: 'string', description: null },
{ name: 'function', type: 'string', description: null },
{ name: 'action', type: 'string', description: null },
{ name: 'caller', type: 'string', description: null },
{ name: 'username', type: 'string', description: null }
*/
// insert/update/delete/read/...
function insert(action, functionType, functionName, oldData, newData, system) {
	var db = cloudant.db.use(_config.dbname);
	var t = trace();
	var now = new Date();
	var doc = {
		_id: utility.dateTime2LocalString(utility.getCurrentHKTime(now)) +
			utility.generateId(_config.id_ext_length),
		dateTime: utility.dateTime2LocalString(utility.getCurrentHKTime(now))
	};

	if (typeof functionType === 'string') {
		doc.functionType = functionType;
	} else if (_config.module_name) {
		doc.functionType = _config.module_name;
	}
	if (typeof functionName === 'string') {
		doc.function = functionName;
	} else if (_config.up_lv) {
		doc.function = t[_config.up_lv + 1].path.replace(/.*\\/g, '').replace(/\.js$/g, '');
	}
	if (typeof action === 'string') {
		doc.action = action;
	}
	// if (typeof delta !== 'undefined' && delta !== null) { doc.delta = delta; }
	if (typeof oldData !== 'undefined') {
		doc.oldData = oldData;
	}
	if (typeof newData !== 'undefined') {
		doc.newData = newData;
	}
	/*if (system != true)
		if (insert.caller) {
			if (insert.caller.caller) {
				doc.caller = insert.caller.caller.name;
			} else {
				doc.caller = insert.caller.name;
			}
	}*/

	var user = contextService.get('request:user');
	if (typeof user !== 'undefined' && user !== null) {
		if (user['userId'] !== undefined) {
			doc.userId = user.userId;
		}
		if (user['username'] !== undefined) {
			doc.userName = user.username;
		}
		if (user['userGroup'] !== undefined) {
			doc.userGroup = user.userGroup;
		}
	}

	var info = contextService.get('request:info');
	if (typeof info !== 'undefined' && info !== null) {
		doc.info = {};
		if (info['ip'] !== undefined) {
			doc.info.ip = info.ip;
		}
	}
	db.insert(doc, function(err, body) {
		if (err) {
			throw err;
		}
	});
}

module.exports = function(config) {
	// if (config) { Object.assign(_config, config); }
	cloudant = Cloudant(config, function(err, cloudant, reply) {
		if (err) {
			throw err;
		}
	});
	return {
		insert: insert
	};
}
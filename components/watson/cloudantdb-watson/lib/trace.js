function trace() {
    var ret = [],
        trace;

    try { throw new Error(); } catch (error) { trace = error.stack; }
    trace.split('\n').slice(1).forEach(function(t) {
        t = t.trim().slice(3);
        var type = null,
            caller = null,
            path = null,
            row = null,
            col = null;
        if (t[t.length - 1] === ')') {
            if (t.replace(/ \(.*\)/g, '').indexOf('.') === -1) {
                caller = t.replace(/ \(.*\)/g, '');
            } else {
                type = t.replace(/\..*/g, '');
                caller = t.replace(/ \(.*\)/g, '').replace(/.*\./g, '');
            }
            var s = t.split(':');
            path = s.slice(0, s.length - 2).join(':').replace(/.*\(/g, '');
            row = s[s.length - 2];
            col = s[s.length - 1].slice(0, s[s.length - 1].length - 1);
        } else {
            var s = t.split(':');
            path = s.slice(0, s.length - 2).join(':');
            row = s[s.length - 2];
            col = s[s.length - 1];
        }

        ret.push({
            full: t,
            type: type,
            caller: caller,
            path: path,
            row: row,
            col: col,
        });
    })
    return ret;
}

module.exports = trace;
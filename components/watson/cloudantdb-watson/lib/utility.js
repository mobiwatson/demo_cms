function generateId(length, possible) {
	if (typeof possible === 'undefined' || possible === null) {
		possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	} else if (typeof possible !== 'string') {
		throw 'typeof possible must be string'
	}

	var text = "";
	for (var i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}


exports = module.exports;
exports.generateId = generateId;
exports.getCurrentHKTime = function(date) {
	return date.toLocaleString("en", {
		timeZone: 'Asia/Hong_Kong'
	})
};
exports.dateTime2LocalString = function(dateTime) {
	var dt;
	if (typeof dateTime === 'string' || typeof dateTime === 'number') {
		dt = new Date(dateTime);
	} else {
		dt = dateTime;
	}
	return dt.getFullYear().toString() + '-' +
		("0" + (dt.getMonth() + 1)).slice(-2) + '-' +
		("0" + dt.getDate()).slice(-2) + ' ' +
		("0" + dt.getHours()).slice(-2) + ':' +
		("0" + dt.getMinutes()).slice(-2) + ':' +
		("0" + dt.getSeconds()).slice(-2);
};
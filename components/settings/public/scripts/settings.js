function getData() {
	// TODO:
	var data = [];
	$.each($('.form>div'), function(key, value) {
		var record = {};
		var _id = $(value).attr('id');
		if (typeof _id !== 'undefined' && _id !== null) {
			record._id = _id;
		}
		$.each($('input', value), function(key, input) {
			var names = $(input).attr('id').split(delimiter);
			var obj = {};
			var input_id = names[0];
			var curr = record;
			for (var i = 1; i < names.length; i++) {
				input_id += delimiter + names[i];
				if ($('#' + input_id).length > 0) {
					if (!$('#' + input_id).data('source')) {
						curr[names[i]] = $('#' + input_id).val();
					}
					break;
				} else {
					if (typeof curr[names[i]] === 'undefined') {
						curr[names[i]] = {};
					}
					curr = curr[names[i]];
				}
			}
		});
		data.push(record);
	});
	return data;
}

$(document).ready(function() {

	$('button[name=update]').click(function() {
		showLoading()
		var data = {
			data: [
				{
					'_id': 'HUMANSPAM',
					'_rev': $('#HUMANSPAM_-__rev').val(),
					'blockTime': $('#HUMANSPAM_-_blockTime').val(),
					'errorMessageTC': $('#HUMANSPAM_-_errorMessageTC').val(),
					'errorMessageEN': $('#HUMANSPAM_-_errorMessageEN').val(),
				},
				{
					'_id': 'IDLE',
					'_rev': $('#IDLE_-__rev').val(),
					'idleTime': $('#IDLE_-_idleTime').val(),
					'idleMsgTC': $('#IDLE_-_idleMsgTC').val(),
					'idleMsgEN': $('#IDLE_-_idleMsgEN').val(),
				}
			],
			watson: {
				'THRESHOLD.HIGH': $('#THRESHOLD_-_HIGH').val(),
				'THRESHOLD.LOW': $('#THRESHOLD_-_LOW').val(),
				'human_spam_max': $('#HUMANSPAM_-_limit').val(),
			},
			_csrf: csrfToken
		}
		$.ajax({
			url: window.location.pathname + "/update?updateId=" + updateId,
			method: 'POST',
			data: data,
			success: function(data, textStatus, jqXHR) {
				hideLoading()
				alert(data);
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				hideLoading()
				alert(jqXHR.responseJSON.message || jqXHR.responseJSON.error);
			}
		})
	})
})
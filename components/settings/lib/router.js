var express = require('express');
var path = require('path');
var utility
var async = require("async");
var router = express.Router();
var service = require('./service');
var auditLog;


// static files
router.get('/assets/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
});
router.get('/scripts/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
})
router.get('/styles/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
});

// page and action
router.use(function(req, res, next) {
	res.locals.baseUrl = req.originalUrl;
	next();
})

router.get('/', function(req, res) {
	service.get(function(err, data) {
		if (err) {
			console.log(req.originalUrl, err);
			throw err;
		}
		// req.session.watson = data.watson;
		// console.log(JSON.stringify(data.watson));
		// console.log(data)
		res.locals.settings = data.db
		res.locals.csrfToken = req.csrfToken()

		res.render(path.join(__dirname, '../views/index'));
	});
});
router.post('/update', function(req, res) {
	async.waterfall([
		(next) => service.update(req.body, next)
	], (err, result) => {
		if (err) {
			if (typeof err == "string") {
				res.status(400).json({
					message: err
				});
			} else {
				res.status(400).json(err);
			}
		} else {
			res.status(200).send('success');

		}
	})
});

function initialize(config) {
	var AuditLog = require(config.cloudant.location).Audit
	auditLog = new AuditLog(config.cloudant);
	utility = require(config.utility);
}

exports = module.exports = router;
exports.initialize = initialize
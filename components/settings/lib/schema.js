module.exports = {
	docs: [
		{
			_id: 'THRESHOLD',
			name: 'Confidence Level',
			fields: [
				{
					name: 'HIGH',
					type: "number",
					description: 'High:',
					subDesc: ">=",
					source: 'watson',
					key: 'THRESHOLD.HIGH'
				},
				{
					name: 'LOW',
					type: 'number',
					description: 'Low:',
					subDesc: "<",
					source: 'watson',
					key: 'THRESHOLD.LOW'
				}
			]
		},
		{
			_id: 'HUMANSPAM',
			name: 'Human Spam Setting',
			fields: [
				{
					name: 'blockTime',
					type: 'number',
					description: 'Block Time (sec):',
				},
				{
					name: 'limit',
					type: 'number',
					description: 'Attempt Threshold:',
					source: 'watson',
					key: 'human_spam_max'
				},{
					name: 'errorMessageTC',
					type: 'string',
					description: "Error Message: ",
					subDesc: "TC",
				},
				{
					name: 'errorMessageEN',
					description: "",
					type: 'string',
					subDesc: "EN",
				},
			]
		},
		{
			_id: 'IDLE',
			name: 'Idle Setting',
			fields: [
				{
					name: 'idleTime',
					type: 'number',
					description: "Idle Time: ",
				},
				{
					name: 'idleMsgTC',
					description: "Idle Message",
					type: 'string',
					subDesc: "TC",
				},
				{
					name: 'idleMsgEN',
					description: "",
					type: 'string',
					subDesc: "EN",
				},
			]
		},
	/*
        {
            _id: 'HUMAN_REDIRECT',
            description: null,
            fields: [
                { name: 'receiver', type: 'array', description: null },
                { name: 'attempt_threshold', type: 'number', description: 'Attempt Threshold', source: 'watson', key: 'email_redirect_count' }
            ]
        },
        {
            _id: 'SURVEY',
            description: 'Survey',
            fields: [
                { name: 'idle_time', type: 'number', description: 'Idle Time' },
                { name: 'daily_limit', type: 'number', description: null }
            ]
        },
        
        
        {
            _id: 'TEMPLATE_THRESHOLD',
            description: 'Template Threshold',
            fields: [
                { name: 'facility_template', type: 'number', description: 'Facility' },
                { name: 'shop_threshold', type: 'number', description: 'Shop' }
            ]
        }*/
	]
}
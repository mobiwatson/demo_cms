var async = require('async');
var request = require('request');
var settingLink,
	cloundant,
	watson;
var schema = require('./schema');

var audit;
var configuration = {
	module: 'settings',
	cloudant: {
		server: {}
	},
	watson: {},
	workspace: {
	}
};

function _getSchemaSourceFields(source) {
	var ret = []
	schema.docs.forEach(doc => {
		doc.fields.forEach(field => {
			if (field.source === source) {
				ret.push({
					name: field.name,
					key: field.key,
					type: field.type
				});
			}
		})
	})
	return ret;
}

function _getWatson(lang) {
	var workspace_id = ''
	if (typeof configuration.workspaceLocation[lang] === 'undefined' || configuration.workspaceLocation[lang] === null) {
		throw 'No this language';
	}
	return watson.initialize({
		watson: configuration.watson,
		workspace_id: configuration.workspaceLocation[lang]
	});
}

function _getSelector(criteria) {
	var qs = {
		selector: {
			'_id': {
				'$gt': null
			}
		},
		sort: [{
			'_id': 'asc'
		}]
	}
	if (typeof criteria === 'string') {
		var filtered = schema.docs.filter(doc => {
			return doc._id === criteria;
		})
		if (filtered.length === 0) {
			throw 'Key: ' + criteria + ' does not exist'
		}
		qs.selector._id['$eq'] = criteria;
	} else if (Array.isArray(criteria)) {
		crtieria.forEach(c => {
			var filtered = schema.docs.filter(doc => {
				return doc.id === c
			});
			if (filtered.length === 0) {
				throw 'Key: ' + c + ' does not exist'
			}
			qs.selector._id['$eq'] = c;
		})
	} else if (criteria !== null) {
		throw 'Does not support criteria typeof ' + typeof criteria;
	}

	return qs;
}

function initialize(config) {
	if (typeof config !== 'Object' && config === null) {
		throw 'Configuration required';
	}
	if (typeof config.cloudant !== 'Object' && config.cloudant === null) {
		throw 'Cloudant Configuration required';
	}
	if (typeof config.watson !== 'Object' && config.watson === null) {
		throw 'Waston Configuration required';
	}
	if (typeof config.workspace !== 'Object' && config.workspace === null) {
		throw 'Workspace Configuration required';
	}
	settingLink = config.chatbotLink + "/setting"
	watson = require(config.watson.location);
	cloundant = require(config.cloudant.location);
	var Audit = cloundant.Audit;
	audit = new Audit(config.cloudant);
	configuration.cloudant.server = config.cloudant;
	configuration.cloudant.dbname = config.database.settings;
	configuration.watson = config.watson;
	configuration.workspace = config.workspace;
	configuration.workspaceLocation = config.workspaceLocation;
}

function get(keys, callback) {
	if (typeof keys === 'function') {
		callback = keys;
		keys = null;
	}

	try {
		var qs = _getSelector(keys);
		var db = cloundant(configuration.cloudant);
		db.find(qs, function(err, data) {

			if (err) {
				throw err;
			}

			var result = [];

			var ret = {
				db: []
			};
			Object.assign(ret.db, schema.docs);

			var mappingData = function() {
				schema.docs.forEach(doc => {
					var setting = data.docs.filter(d => {
						return d._id === doc._id;
					})[0];
					doc.fields.forEach(field => {
						if (field.source === 'watson') {
							field.value = watsonFields.filter(wf => {
								return wf.name === field.name
							})[0].value;
						} else if (setting) {
							doc._rev = setting._rev;
							var value = setting[field.name];
							if (value) {
								field.value = value;
							}
						}
					});
				});
				callback(err, ret);
			}

			// check if value from watson
			var watsonFields = _getSchemaSourceFields('watson');
			if (watsonFields.length > 0) {
				var watson = _getWatson('tc');
				watson.dialog.getOne({
					dialog_node: 'RESET_VAR'
				}, function(err, data) {
					if (err) {
						console.log(err)
						callback(err);
					} else {
						// console.log(JSON.stringify(data.context));
						if (data.context) {
							watsonFields.forEach(wf => {
								var fs = wf.key.split('.');
								if (fs.length > 0) {
									var looking = data.context;
									fs.forEach(f => {
										try {
											if (typeof looking[f] === wf.type) {
												wf.value = looking[f]; return;
											} else {
												looking = looking[f];
											}
										} catch (err) {}
									});
								}
							});
							ret.watson = data;
						}
						mappingData();
					}
				});
			} else {
				mappingData();
			}
		});
	} catch (err) {
		callback(err);
	}
}

function _updateWatsonDialogContextValue(lang, fields, callback) {
	var watson = _getWatson(lang);
	watson.dialog.getOne({
		dialog_node: 'RESET_VAR'
	}, function(err, dialog) {
		if (err) {
			callback(err);
		} else {
			if (typeof dialog.context !== 'undefined') {
				try {
					fields.forEach(field => {
						var fieldKeys = field.key.split('.');
						// update dialog context
						if (fieldKeys.length === 1) {
							if (field.value == undefined)
								throw fieldKeys[0] + " should not be empty";
							if (typeof dialog.context[fieldKeys[0]] === 'undefined' || typeof dialog.context[fieldKeys[0]] === field.type) {
								dialog.context[fieldKeys[0]] = field.value;
							} else
								throw "wrong type"

						} else if (fieldKeys.length === 2) {
							if (field.value == undefined)
								throw fieldKeys[0] + " " + fieldKeys[1] + " should not be empty";
							if (typeof dialog.context[fieldKeys[0]][fieldKeys[1]] === 'undefined' || typeof dialog.context[fieldKeys[0]][fieldKeys[1]] === field.type) {
								dialog.context[fieldKeys[0]][fieldKeys[1]] = field.value;
							} else
								throw "wrong type"
						}
					});
					// console.log('email_redirect_count', dialog.context['email_redirect_count']);
					// console.log('human_spam_max', dialog.context['human_spam_max']);
					// console.log('CONF_THRESHOLD.HIGH', dialog.context['CONF_THRESHOLD']['HIGH']);
					// console.log('CONF_THRESHOLD.MIDDLE', dialog.context['CONF_THRESHOLD']['MIDDLE']);
					// console.log('CONF_THRESHOLD.CHG_INT', dialog.context['CONF_THRESHOLD']['CHG_INT']);
					watson.dialog.update({
						dialog_node: 'RESET_VAR',
						context: dialog.context
					}, callback);
				} catch (err) {
					callback(err);
				}
			} else {
				throw 'No Context';
			}
		}
	});
}

function _updateDataBase(data, callback) {
	var db = cloundant(configuration.cloudant);
	async.waterfall([
		(next) => db.insert_bulk(data, next),
		(data, next) => updateMiddleware(data, next)
	], callback)
}
function updateMiddleware(data, callback) {
	console.log(settingLink)
	request({
		method: 'POST',
		url: settingLink,
		headers: {
			'content-type': 'application/json',
			'accept-encoding': 'UTF-8'
		},
		body: {},
		json: true
	}, (err, res) => {
		console.log(err)
		console.log(res)
		callback(err, res)
	});
}
function update(data, callback) {
	var watsonFields = _getSchemaSourceFields('watson');
	try {
		if (watsonFields.length > 0 && data.watson) {
			Object.keys(data.watson).forEach(key => {
				var field = watsonFields.filter(field => {
					return field.key === key
				})[0];
				var value = data.watson[key];
				if (field.type === 'string') {
					value = data.watson[key].toString();
				}
				if (field.type === 'number') {
					value = Number(data.watson[key]);
				}
				field.value = value;
			});
		}

		data.data.forEach(data => {
			var filtered = schema.docs.find(doc => doc._id === data._id);
			if (!filtered) {
				throw 'Invalid document id: ' + data._id;
			}
			filtered.fields.forEach(field => {
				if (typeof data[field.name] !== 'undefined' && data[field.name] !== null) {
					if (field.type === 'string') {
						data[field.name] = data[field.name].toString();
					}
					if (field.type === 'number') {
						data[field.name] = Number(data[field.name]);
					}
					if (field.check) {
						if (field.check.regex) {
							if (data[field.name].indexOf(field.check.regex) == -1)
								throw (field.name + " should contain " + field.check.regex)
						}
					}
				} else if (field.source != "watson")
					throw field.description + (field.subDesc ? field.subDesc : '') + " is empty";
			})
		});

		// update each watson
		async.parallel([
			(next) => _updateWatsonDialogContextValue('en', watsonFields, next),
			(next) => _updateWatsonDialogContextValue('tc', watsonFields, next),
			(next) => _updateDataBase(data.data, next),
		], callback);
	} catch (err) {
		callback(err);
	}
}

exports = module.exports;
exports.initialize = initialize;
exports.get = get;
exports.update = update;
module.exports = {
	moduleGroup: undefined,
	item: [{
		name: "settings",
		module: {
			initialize: (config) => {
				require('./lib/router').initialize(config)
				require('./lib/service').initialize(config)
			},
			router: require('./lib/router'),
			menu: require('./lib/menu')
		}
	}]
};

var curr_lang = 'en';
var table;
var currentParam;


function addExampleControl(wrapper, value) {
	var editorContainer = $(wrapper);
	var last_input = $('div.input-group:last-child input[type=text]', editorContainer).val();
	if (last_input === "") {
		return;
	}

	var ctrl = [
		'<div class="input-group">',
		'<input type="text" aria-label="Text input with checkbox" class="editItemRow_input1" value="' + value + '">',
		'<span><i aria-hidden="true" class="fa fa-times remove_self" onclick="removeExampleControl(this);"></i></span>',
		'</div>'
	].join("");
	editorContainer.append(ctrl);
}

function removeExampleControl(ele) {
	$(ele).parent().parent().remove();
}

$(document).ready(function() {
	// $('#sintent').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	// $('#skeyword').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	$('#clear').click(function() {
		$('#sintent').val("");
		$('#skeyword').val("");
	});
	$('#search').click(function() {
		// need redo

		table.ajax.reload();
	});

	table = $('table').DataTable({
		'ordering': true,
		'serverSide': false,
		'paging': false,
		'searching': false,
		'processing': true,
		'autoWidth': true,
		'ajax': {
			'timeout': 30000,
			'url': window.location.pathname + '/list',
			'data': function(data) {
				showLoading()
				data.lang = curr_lang;
				data.cs = {};
				if ($('#sintent').val() !== '' && $('#sintent').val() !== null) {
					data.cs._id = $('#sintent').val();
				}
				if ($('#sbestquestion').val() !== '' && $('#sbestquestion').val() !== null) {
					data.cs.descriptiontcen = $('#sbestquestion').val();
				}
				if ($('#skeyword').val() !== '' && $('#skeyword').val() !== null) {
					data.cs.samples$tcen = $('#skeyword').val();
				}
				currentParam = data;
				return data;
			},
			"dataSrc": function(json) {
				//Make your callback here.
				hideLoading()
				updateId = json.updateId;
				return json.data;
			}
		},
		"order": [
			[2, "asc"]
		],
		'columns': [
			{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'className': 'checkbox',
				'render': function(data, type, full, meta) {
					return '<input type="checkbox" name="id_' + meta.row + '" data-intent="' + full._id + '">';
				}
			},
			{
				'targets': 1,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					return '<a href="#" role="button" data-toggle="modal" data-target="#detailmodal" data-row="' + meta.row + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
				}
			},
			{
				"data": '_id',
				"type": "string",
				"render": function(data, type, full, meta) {
					return data ? '#' + data : ''
				}
			},
			{
				"data": 'EN',
				"type": "string",
				"render": function(data, type, full, meta) {
					return data || ''
				}
			},
			{
				"data": 'TC',
				"type": "string",
				"render": function(data, type, full, meta) {
					return data || ''
				}
			},
			{
				"data": 'ENSamples',
				"type": "number",
				"render": function(data, type, full, meta) {
					return data ? data.length : 0;
				}
			},
			{
				"data": 'TCSamples',
				"type": "number",
				"render": function(data, type, full, meta) {
					return data ? data.length : 0;
				}
			},
		],
	});

	$('#select-all').on('click', function() {
		// Check/uncheck all checkboxes in the table
		var rows = table.rows({
			'search': 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});

	$('tbody').on('change', 'input[type="checkbox"]', function() {
		// If checkbox is not checked
		if (!this.checked) {
			var el = $('#select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if (el && el.checked && ('indeterminate' in el)) {
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
		}
	});



	$('#detailmodal').on('show.bs.modal', function(event) {

		// Button that triggered the modal
		// Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		// var modal = $(this)
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body input').val(recipient)
		var button = $(event.relatedTarget) // Button that triggered the modal
		var row = button.data('row');
		var data = table.row(row).data();
		$(this).data('row', row);

		$('#fintent').text('#' + decode(data._id));
		$("#ftc").val(decode(data.TC));
		$("#fen").val(decode(data.EN));
		if (data.TCSamples) {
			data.TCSamples.sort(function(a, b) {
				var nameA = a.toUpperCase();
				var nameB = b.toUpperCase();
				if (nameA < nameB) {
					return -1;
				}
				if (nameA > nameB) {
					return 1;
				}
			}).forEach(function(e) {
				addExampleControl("#ftcsamples", e);
			});
		}
		if (data.ENSamples) {
			data.ENSamples.sort(function(a, b) {
				var nameA = a.toUpperCase();
				var nameB = b.toUpperCase();
				if (nameA < nameB) {
					return -1;
				}
				if (nameA > nameB) {
					return 1;
				}
			}).forEach(function(e) {
				addExampleControl("#fensamples", e);
			});
		}

	})
	$('#detailmodal').on('hide.bs.modal', function() {
		$('#fintent').text('');
		$('#fcreated').text('');
		$('#fupdated').text('');
		$("#detailmodal input").val("");
		$('#ftcsamples').empty();
		$('#fensamples').empty();

	});
	$('button[name=import]').click(function() {
		var button = document.createElement('button');
		var input = document.createElement('input');
		var form = document.createElement('form');

		form.setAttribute('enctype', 'multipart/form-data');
		form.setAttribute('method', 'POST');
		form.setAttribute('action', 'javascript:;');
		form.setAttribute('role', 'form');

		input.setAttribute('type', 'file');
		input.setAttribute('name', 'files');
		input.setAttribute('accept', '.xlsx');

		form.appendChild(input);
		form.appendChild(button);

		document.body.appendChild(form);

		input.onchange = function() {
			showLoading()
			button.click();
		}
		button.onclick = function() {
			var formData = new FormData($(form)[0]);
			formData.append("_csrf", csrfToken)
			$.ajax({
				'timeout': 30000,
				url: window.location.pathname + '/import?updateId=' + updateId,
				type: 'POST',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {
					hideLoading()

					alert('Import success!');

					table.ajax.reload();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status === 0 || jqXHR.readyState === 0) {
						return;
					}
					hideLoading()

					alert(jqXHR.responseJSON.message);
				}
			});
		}
		input.click();
	});
	$('button[name=exportselected]').click(function() {
		if ($('tbody input[type=checkbox]:checked').length === 0) {

			alert('no intent selected!');

		} else {
			showLoading()
			var intents = [];
			$.each($('tbody input[type=checkbox]:checked'), function(index, value) {
				intents.push($(value).data('intent'));
			});
			var a = document.createElement("a");
			a.href = window.location.pathname + '/export?selected=' + intents.join(",");
			document.body.appendChild(a);
			a.click();
			hideLoading()

		}
	});
	$('button[name=exportall]').click(function() {
		showLoading()
		var reqParam = currentParam;
		var a = document.createElement("a");
		a.href = window.location.pathname + '/export?' + $.param(reqParam);
		// a.target = "_blank";
		document.body.appendChild(a);
		a.click();
		hideLoading()
	});
	$('button[name=addExample]').click(function() {
		addExampleControl(this.attributes.getNamedItem("data").value, "");
	})
	$('button[name=save]').click(function() {
		showFullPageLoading()
		var oldData = table.row($('#detailmodal').data('row')).data();
		var data = {
			_csrf: csrfToken,
			_id: oldData._id,
			_rev: oldData._rev,
			TC: $('#ftc').val(),
			EN: $('#fen').val(),
			TCSamples: [],
			ENSamples: []
		}
		$.each($('#ftcsamples').children(), function(key, value) {
			data.TCSamples.push($('input[type=text]', value).val());
		});
		$.each($('#fensamples').children(), function(key, value) {
			data.ENSamples.push($('input[type=text]', value).val());
		});

		
		$.ajax({
			'timeout': 30000,
			url: window.location.pathname + '/save?updateId=' + updateId,
			method: 'POST',
			data: data,
			success: function(data, textStatus, jqXHR) {
				hideFullPageLoading()
				alert(data);
				$('#detailmodal').modal('hide');
				table.ajax.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (jqXHR.status === 0 || jqXHR.readyState === 0) {
					return;
				}
				hideFullPageLoading()
				alert(jqXHR.responseJSON.message);
			}
		})
	})
})
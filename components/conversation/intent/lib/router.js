var express = require('express');
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var multipart = require('connect-multiparty');
var async = require('async');
var excel = require('node-excel-export');
var xlsx = require('xlsx');
var update = {
	User: undefined,
	Id: undefined
};
var multipartMiddleware = multipart();

var service = require('./service');

var router = express.Router();

var auditLog;
var utility;

// static files
router.get('/assets/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
});
router.get('/scripts/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
})
router.get('/styles/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
});

// page and action
router.get('/', function(req, res) {
	res.locals.baseUrl = req.originalUrl;
	res.render(getViewPath('/index'), {
		csrfToken: req.csrfToken(),
		updateId: update.Id
	});
});
router.get('/list', function(req, res) {
	async.waterfall([
		(next) => service.getData(next, utility.getParams(req.query))
	], function(err, result) {
		if (err) {
			res.status(400).send(err);
		} else if (utility.HTMLfilter(result) instanceof Error) {
			res.status(400).json(utility.HTMLfilter(result));
		} else {
			res.send({
				"draw": req.query.draw,
				"recordsTotal": result.length,
				"recordsFiltered": result.length,
				"data": utility.HTMLfilter(result),
				"updateId": update.Id
			});
		}
	})
})
router.get('/export', function(req, res) {
	async.waterfall([
		(next) => service.getData(next, utility.getParams(req.query)),
		(data, next) => service.convertToXLSX(next, data)
	], (err, result) => {
		if (err) {
			res.status(400).send(err);
		} else {
			res.set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			res.attachment('Intent_' + utility.dateTime2LocalString(utility.getCurrentHKTime(new Date())) + '.xlsx');
			res.status(200).send(result);
		}
	})
});
router.post('/import', multipartMiddleware, function(req, res) {
	var temp = update
	delete req.body._csrf

	async.waterfall([

		(next) => service.XLSXToArray(next, req.files.files.path),
		(intents, next) => service.update(next, intents)
	], function(err, result) {

		if (err) {
			if (typeof err == "string") {
				res.status(400).json({
					message: err
				});
			} else {
				res.status(400).json(err);
			}
		} else {
			// update intent to watson conversation
			res.status(200).json('Import success!');
		}
	})
});
router.post('/save', function(req, res, next) {
	var temp = update
	delete req.body._csrf
	async.waterfall([
		(next) => service.update(next, [req.body])
	], (err, result) => {
		if (err) {
			if (typeof err == "string") {
				res.status(400).json({
					message: err
				});
			} else {
				res.status(400).json(err);
			}
		} else {
			res.status(200).json('Save ' + req.body._id + ' success!');
		}
	})



});
/*
router.get('/:intent', function(req, res) {
	var params = {
		lang: 'en',
		export: true,
		intent: req.params.intent
	};

	service.get(params, function(err, data) {
		res.send(data);
	});
});*/

function getViewPath(view) {
	return path.join(__dirname, '../views' + view);
}

function initialize(config) {
	if (typeof config !== 'object') {
		throw 'invalid config';
	}
	var AuditLog = require(config.cloudant.location).Audit;
	utility = require(config.utility);
	update.Id = utility.generateId(10);

	auditLog = new AuditLog(config.cloudant);
}


exports = module.exports = router;
exports.initialize = initialize;

var _config = {

	workspace: {
		en: '',
		tc: ''
	}
};
var cloudantdb;

var xlsx = require('xlsx');
var xlsxExport = require('node-excel-export');
var async = require("async");
var utility;

function initialize(config) {
	if (typeof config !== 'object') {
		throw 'Invalid config';
	}
	cloudantdb = require(config.cloudant.location);
	utility = require(config.utility);
	Object.assign(_config, config);
}

function getWatson(lang) {
	if (typeof _config.workspaceLocation[lang] === 'undefined' || _config.workspaceLocation[lang] === null) {
		throw 'No this language';
	}
	return require(_config.watson.location).initialize({
		watson: _config.watson,
		workspace_id: _config.workspaceLocation[lang]
	});
}


function getData(callback, criteria) {
	try {
		if (typeof callback !== 'function') {
			throw 'no callback' ;
		}

		// if (typeof criteria.skip !== 'undefined' && criteria.skip !== null) { qs.skip = parseInt(criteria.skip); }
		// if (typeof criteria.take !== 'undefined' && criteria.take !== null) { qs.limit = parseInt(criteria.take); }
		var qs = utility.getQueryString(criteria);
		var docs;
		//console.log(JSON.stringify(qs));
		var db = cloudantdb({
			server: _config.cloudant,
			dbname: _config.database["intent"]
		}, callback);
		// console.log(criteria);
		// console.log(JSON.stringify(qs));
		// if (typeof qs.selector["$or"] !== 'undefined') { console.log(qs.selector["$or"]); }
		db.find(qs, function(err, data) {
			if (err) callback(err);
			if (data.error) callback(data.error);
			else callback(null, data.docs);
		});
	} catch (err) {
		callback(err, null);
	}
}


function convertToXLSX(callback, intents) {
	try {
		intents.sort((a, b) => {
			if (a._id.toUpperCase() < b._id.toUpperCase()) {
				return -1;
			}
			if (a._id.toUpperCase() > b._id.toUpperCase()) {
				return 1;
			}
			return 0;
		});

		var specification = {
			type: { // <- the key should match the actual data key 
				displayName: 'Type',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120 // <- Here you specify the column header 
			},
			_id: {
				displayName: 'Intent(id)',
				headerStyle: utility.xlsxFormat.headerRed,
				cellStyle: utility.xlsxFormat.cellRed,
				width: 120
			},
			description: {
				displayName: 'Description',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			count: {
				displayName: 'Count',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			sample: {
				displayName: 'Sample',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
		};
		var result = ["TC", "EN"].map(_lang => {
			var docs = [];
			var count = 0;
			intents.forEach(intent => {
				// summary
				var summary = {
					type: 'summary',
					_id: intent._id,
					description: intent[_lang],
					count: (intent[_lang + "Samples"] === undefined || intent[_lang + "Samples"] === null) ? 0 : intent[_lang + "Samples"].length
				}
				docs.push(summary);

				if (!(intent[_lang + "Samples"] === undefined || intent[_lang + "Samples"] === null)) {

					intent[_lang + "Samples"].sort((a, b) => {
						if (a.toUpperCase() < b.toUpperCase()) {
							return -1;
						}
						if (a.toUpperCase() > b.toUpperCase()) {
							return 1;
						}
						return 0;
					});
					for (var i = 0; i < summary.count; i++) {
						var sample = {
							type: 'sample',
							_id: intent._id,
							sample: intent[_lang + "Samples"][i]
						}
						docs.push(sample);
					}
				}

			});
			return {
				name: _lang, // <- Specify sheet name (optional) 
				specification: specification, // <- Report specification 
				data: docs // <-- Report data 
			}
		})

		callback(null, xlsxExport.buildExport(result));
	} catch (ex) {
		callback(ex)
	}
}
function update(callback, intents) {
	async.waterfall([
		(next) => getData(next, {
			search: {
				_id: intents.map(_i => _i._id)
			}
		}),
		(data, next) => {
			var updateIntents = intents.slice();
			var errMessage = [];
			updateIntents.forEach(_i => {
				if (_i.ENSamples && _i.ENSamples.filter((value, index, self) => self.indexOf(value) !== index).length > 0) {
					errMessage.push(_i._id + " examples repeated")
				}
				if (_i.TCSamples && _i.TCSamples.filter((value, index, self) => self.indexOf(value) !== index).length > 0) {
					errMessage.push(_i._id + " examples repeated")
				}
				var temp = data.find((_d) => _d._id == _i._id)
				if (temp != undefined) {
					_i._rev = temp._rev
				}
			})
			if (errMessage.length > 0) {
				next(errMessage.join("\n"))
			} else {
				next(null, updateIntents)
			}
		},
		(data, next) => async.parallel([
			(next) => updateDataBase(next, data),
			(next) => async.parallelLimit(data.map((_intent) => (callback) => updateWatson(_intent, callback)), 25, next)
		], (err, res) => {
			if (err) next(err)
			else next(null, res)
		})
	], callback)
}

function updateDataBase(callback, docs) {
	var db = cloudantdb({
		server: _config.cloudant,
		dbname: _config.database["intent"]
	}, callback);
	db.insert_bulk(docs, (err, data) => {
		if (err) callback(err);
		else callback(null, data)
	});
}

function updateWatson(intent, callback) {
	if (typeof intent === 'undefined' || intent === null) {
		callback(new Error('Parameters required'));
	} else if (typeof intent._id === 'undefined' && intent._id === null) {
		callback('intent.old_intent required');
	} else {
		async.parallel(
			["TC", "EN"].map(_lang => (next) => {
				var _intent = {
					intent: intent._id,
					description: intent[_lang],
					examples: intent[_lang + "Samples"] ? intent[_lang + "Samples"].map(s => {
						return {
							text: s
						};
					}) : []
				};
				if (_intent.intent && _intent.intent.trim() != "")
					_intent.examples.push({
						text: (_lang == "EN" ? "CMD_BTN " : "CMD_按鍵") + _intent.intent
					})
				var watson = getWatson(_lang.toLowerCase());
				watson.intent.update(_intent, (err, res) => {
					if (err) {
						watson.intent.create(_intent, next)
					} else next(null, res)
				});
			}),
			callback)
	}
}

function XLSXToArray(callback, path) {
	try {
		var data = [];
		var docs = xlsx.readFile(path);
		docs.Workbook.Sheets.forEach(_ws => {
			var lang = _ws.name
			var ws = docs.Sheets[_ws.name];
			var address = ws["!ref"];

			var header = {
				type: "Type",
				_id: "Intent(id)",
			};
			header[lang] = "Description"
			header[lang + "Samples"] = "Sample"
			var match = {};


			var maxCol = address.match(/A1:([A-Z]*)\d*/)[1];
			var maxRow = address.match(/A1:[A-Z]*(\d*)/)[1];
			for (var i = 0; i <= utility.ColumnToInt(maxCol); i++) {
				var column = utility.IntToColumn(i);
				if (!ws[column + "1"])
					continue
				Object.keys(header).forEach(_h => {
					if (header[_h] == ws[column + "1"].v) {
						if (match[_h] != undefined) {
							throw "Have Same Column: " + header[_h];
						} else {
							if (match[_h] != undefined) {
								match[_h] += "," + column;
							} else {
								match[_h] = column;
							}
						}
					}
				});
			}
			var notMatch = Object.keys(header).filter(_h => Object.keys(match).indexOf(_h) == -1);

			if (notMatch.length > 0) {
				throw notMatch.map(_h => header[_h]).join(",") + " not found";
			}

			for (var i = 1; i <= maxRow; i++) {
				if (typeof ws[match._id + i] == "undefined" || ws[match._id + i].v.trim() == "") {
					continue;
				}

				if (i != 1) {
					if (ws[match.type + i].v.trim() == "summary") {
						if (data.findIndex(_d => _d._id == ws[match._id + i].v) != -1) {
							var temp = data.find(_d => _d._id == ws[match._id + i].v)
							if (temp[lang])
								throw "summary should not repeat"
							temp[lang] = ws[match[lang] + i] ? ws[match[lang] + i].v : "",
							temp[lang + "Samples"] = []
						} else {
							var temp = {
								_id: ws[match._id + i].v,
							}
							temp[lang] = ws[match[lang] + i] ? ws[match[lang] + i].v : "",
							temp[lang + "Samples"] = []
							data.push(temp);
						}
					} else if (ws[match.type + i].v.trim() == "sample") {
						if (typeof ws[match[lang + "Samples"] + i] == "undefined" || ws[match[lang + "Samples"] + i].v.trim() == "") {
							continue;
						}
						var originalDataIndex = data.findIndex(_d => _d._id == ws[match._id + i].v);
						if (originalDataIndex != -1) {

							data[originalDataIndex][lang + "Samples"].push(ws[match[lang + "Samples"] + i].v.toString().trim());
						} else {
							console.log(i)
							throw "Can't find summary of #" + ws[match._id + i].v;
						}
					} else {
						throw "Invalid type at row" + i;
					}
				}
			}
		})

		callback(null, data);
	} catch (err) {
		callback(err, null);
	}
}

exports = module.exports;
exports.initialize = initialize;
exports.getData = getData;
exports.update = update;
exports.XLSXToArray = XLSXToArray;
exports.convertToXLSX = convertToXLSX;

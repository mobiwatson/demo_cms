'use strict';

var router = require('./lib/router');
var menu = require('./lib/menu');
var service = require('./lib/service');
// var _config = {
//     database: {
//         conversation: 'convlog',
//         audit: 'audit',
//         survey: 'ranking'
//     }
// }

function initialize(config) {
    router.initialize(config);
    service.initialize(config);
}

exports.initialize = initialize;
exports.router = router;
exports.menu = menu;
var table;
var currentParam;

function resizeTextArea(textarea) {
	var offset = $(textarea)[0].offsetHeight - $(textarea)[0].clientHeight;
	$(textarea).css('height', 'auto').css('height', $(textarea)[0].scrollHeight + offset);
}

function addExampleControl(wrapper, value) {
	var editorContainer = $(wrapper);
	var last_input = $('div.input-group:last-child input[type=text]', editorContainer).val();
	if (last_input === "") {
		return;
	}

	var ctrl = [
		'<div class="input-group">',
		'<input type="text" aria-label="Text input with checkbox" class="editItemRow_input1" value="' + value + '">',
		'<span><i aria-hidden="true" class="fa fa-times remove_self" onclick="removeExampleControl(this);"></i></span>',
		'</div>'
	].join("");
	editorContainer.append(ctrl);
}

function addButtonControl(wrapper, value) {
	var editorContainer = $(wrapper);
	var last_input = $('div.input-group:last-child input[type=text]', editorContainer).val();
	if (last_input === "") {
		return;
	}
	var ctrl = [
		'<div class="input-group">',
		'EN: <input type="text" aria-label="Text input with checkbox" class="editItemRow_input2" data-target="EN" value="' + value.EN + '">',
		'TC: <input type="text" aria-label="Text input with checkbox" class="editItemRow_input2" data-target="TC" value="' + value.TC + '">',
		'playload: <input type="text" aria-label="Text input with checkbox" class="editItemRow_input2" data-target="payload" value="' + value.payload + '">',
		'<span><i aria-hidden="true" class="fa fa-times remove_self" onclick="removeExampleControl(this);"></i></span>',
		'</div>'
	].join("");
	editorContainer.append(ctrl);
}

function removeExampleControl(ele) {
	var editorContainer = $('#editItemRow_wrapper');
	$(ele).parent().parent().remove();
}


$(document).ready(function() {
	$(window).scroll(function() {
		var aTop = $('#sticky').height();
		if ($(this).scrollTop() >= aTop) {
			alert('header just passed.');
		// instead of alert you can use to show your ad
		// something like $('#footAd').slideup();
		}
		var sticky = table.offsetTop;
		console.log(sticky)
		if (window.pageYOffset >= sticky) {
			$('#sticky').addClass("sticky")
		} else {
			$('#sticky').removeClass("sticky");
		}
	});

	// if (typeof $.datetimepicker !== 'undefined') {
	// 	$('#sfromstartdate').datetimepicker({
	// 		format: 'Y-m-d H:00:00'
	// 	});
	// 	$('#stostartdate').datetimepicker({
	// 		format: 'Y-m-d H:00:00'
	// 	});
	// 	$('#sfromexpdate').datetimepicker({
	// 		format: 'Y-m-d H:00:00'
	// 	});
	// 	$('#stoexpdate').datetimepicker({
	// 		format: 'Y-m-d H:00:00'
	// 	});
	// }
	// $('#sstartdate').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	// $('#sexpdate').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	// $('#sans_id').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	// $('#scat').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	// $('#sintent').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	// $('#sans').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });
	// $('#sbq_ans').on('keypress', function(e) {
	// 	if (e.which === 13) {
	// 		$('#search').click()
	// 	}
	// });

	// // ******************************************************
	// // search bar
	$('#search').click(function(e) {
		table.ajax.reload();
	})
	$('#clear').click(function(e) {
		$('#sans_id').val("");
		$('#sintent').val("");
		$('#sans').val("");
	});
	$('button[name=addExample]').click(function() {
		addExampleControl(this.attributes.getNamedItem("data").value, "");
	});
	$('button[name=addButton]').click(function() {
		addButtonControl(this.attributes.getNamedItem("data").value, "");
	});
	// import & export
	$('button[name=import]').click(function() {
		showLoading()
		var button = document.createElement('button');
		var input = document.createElement('input');
		var form = document.createElement('form');

		form.setAttribute('enctype', 'multipart/form-data');
		form.setAttribute('method', 'POST');
		form.setAttribute('action', 'javascript:;');
		form.setAttribute('role', 'form');

		input.setAttribute('type', 'file');
		input.setAttribute('name', 'files');
		input.setAttribute('accept', '.xlsx');

		form.appendChild(input);
		form.appendChild(button);

		document.body.appendChild(form);

		input.onchange = function() {
			button.click();
		}
		button.onclick = function() {
			var formData = new FormData($(form)[0]);
			formData.append("_csrf", csrfToken)
			$.ajax({
				'timeout': 30000,

				url: window.location.pathname + '/import?updateId=' + updateId,
				type: 'POST',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {
					alert('Import success!');
					table.ajax.reload();
					hideLoading()

				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status === 0 || jqXHR.readyState === 0) {
						return;
					}
					hideLoading()
					alert(jqXHR.responseJSON.message);
				}
			});
		}
		input.click();
	});
	$('button[name=exportselected]').click(function() {
		showLoading()
		if ($('tbody input[type=checkbox]:checked').length === 0) {
			alert('no record selected!');
		} else {
			var _ids = [];
			$.each($('tbody input[type=checkbox]:checked'), function(index, value) {
				_ids.push(table.row($(value).data('row')).data()._id);
			});
			var a = document.createElement("a");
			a.href = window.location.pathname + '/export?selected=' + _ids.join(",");
			document.body.appendChild(a);
			a.click();
			hideLoading()
		}
	});
	$('button[name=exportall]').click(function() {
		showLoading()
		var reqParam = $.extend({}, currentParam);

		delete reqParam.skip;
		delete reqParam.limit;

		var a = document.createElement("a");
		a.href = window.location.pathname + '/export?' + $.param(reqParam);
		// a.target = "_blank";
		document.body.appendChild(a);
		a.click();
		hideLoading()

	});

	// ******************************************************
	// in table
	table = $('table').DataTable({
		'ordering': true,
		'serverSide': false,
		'searching': false,
		'processing': true,
		'autoWidth': true,
		'paging': false,
		'ajax': {
			'timeout': 30000,
			'url': window.location.pathname + '/list',
			'data': function(d) {
				showLoading();
				d.cs = {};
				if ($('#sans_id').val() !== null && $('#sans_id').val() !== "") {
					d.cs._id = $('#sans_id').val();
				}
				if ($('#sintent').val() !== null && $('#sintent').val() !== "") {
					d.cs.intent = $('#sintent').val();
				}
				if ($('#sans').val() !== null && $('#sans').val() !== "") {
					d.cs.TCEN = $('#sans').val().replace(/</, "$lt;").replace(/>/, "$gt;");
				}


				currentParam = d;
				return d;
			},
			"dataSrc": function(json) {
				hideLoading()
				updateId = json.updateId;
				return json.data;
			},
		},
		'order': [2, 'asc'],
		'columns': [
			{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'className': 'checkbox',
				'render': function(data, type, full, meta) {
					return '<input type="checkbox" data-row="' + meta.row + '">';
				}
			},
			{
				'targets': 1,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					return '<a href="#" role="button" data-toggle="modal" data-target="#detailmodal" data-row="' + meta.row + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
				}
			},
			{
				"data": '_id',
				"type": "string"
			},
			{
				"data": 'intent',
				"type": "string",
				'render': function(data) {
					return data || ""
				}
			},
			{
				"data": 'remarks',
				"type": "string",
				'render': function(data) {
					return data || ""
				}
			},
			{
				"data": 'EN',
				"type": "string",
				'render': function(data) {
					if (data) return data.toString() > 20 ? data.toString().substring(0, 20) + '...' : data
					else return ""
				}
			},
			{
				"data": 'TC',
				"type": "string",
				'render': function(data) {
					if (data) return data.toString() > 20 ? data.toString().substring(0, 20) + '...' : data
					else return ""
				}
			}
		],
	});

	$('#select-all').on('click', function() {
		// Check/uncheck all checkboxes in the table
		var rows = table.rows({
			'search': 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});
	$('tbody').on('change', 'input[type="checkbox"]', function() {
		// If checkbox is not checked
		if (!this.checked) {
			var el = $('#select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if (el && el.checked && ('indeterminate' in el)) {
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
		}
	});

	// ******************************************************
	// modal
	$('#detailmodal').on('show.bs.modal', function(event) {
		console.log
		var button = $(event.relatedTarget);
		var row = button.data('row');

		var data = table.row(row).data();
		$(this).data('row', row);
		$('#fid').text("Answer Id: " + data._id);
		$("#fintent").text("Intent: " + data.intent);
		$('#fremarks').val(data.remarks);

		data.EN.forEach(function(element) {
			addExampleControl('#fen', element);
		});
		data.TC.forEach(function(element) {
			addExampleControl('#ftc', element);
		});
		if (data.choices)
			data.choices.forEach(function(element) {
				console.log(element)
				addButtonControl('#fchoices', element);
			});
	});

	$('#detailmodal').on('hide.bs.modal', function() {
		$('textarea', this).text('');
		$('input', this).val('');
		$('#fen').empty();
		$('#ftc').empty();
		$('#fchoices').empty();
	});
	$('button[name=back]').click(function() {
		$('#detailmodal').modal('hide');
	});
	$('button[name=save]').click(function() {
		showFullPageLoading()
		var oldData = table.row($('#detailmodal').data('row')).data();
		var data = {
			_csrf: csrfToken,
			_id: oldData._id,
			_rev: oldData._rev,
			intent: oldData.intent,
			remarks: $('#fremarks').val(),
			TC: [],
			EN: [],
			choices: [],
		}
		$.each($('#ftc').children(), function(key, value) {
			data.TC.push($('input[type=text]', value).val());
		});
		$.each($('#fen').children(), function(key, value) {
			data.EN.push($('input[type=text]', value).val());
		});
		$.each($('#fchoices').children(), function(key, value) {
			data.choices.push({
				TC: $('input[data-target=TC]', value).val(),
				EN: $('input[data-target=EN]', value).val(),
				payload: $('input[data-target=payload]', value).val()
			});
		});
		$.ajax({
			url: window.location.pathname + '/save?updateId=' + updateId,
			'timeout': 30000,
			method: 'POST',
			data: data,
			success: function(data, textStatus, jqXHR) {
				alert(data);
				$('#detailmodal').modal('hide');
				table.ajax.reload();
				hideFullPageLoading()
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (jqXHR.status === 0 || jqXHR.readyState === 0) {
					return;
				}
				hideFullPageLoading()
				alert(jqXHR.responseJSON.message);
			}
		})
	})
})
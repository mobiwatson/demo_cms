var configuration = {};

var express = require('express');
var path = require('path');
var async = require('async');
var multipart = require('connect-multiparty');
var fs = require('fs');
var update = {
	User: undefined,
	Id: undefined
};

var router = express.Router();
var async = require('async');
var multipartMiddleware = multipart();
var service = require('./service');
var auditLog;
var utility;

// static files
router.get('/assets/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
});
router.get('/scripts/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
})
router.get('/styles/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
});

// page and action
router.get('/', function(req, res) {
	res.locals.baseUrl = req.originalUrl;
	res.render(getViewPath('/index'), {
		csrfToken: req.csrfToken(),
		updateId: update.Id
	});
});

router.get('/list', function(req, res) {
	var params = utility.getParams(req.query);
	async.waterfall([
		(next) => service.getData(next, params)
	], function(err, result) {
		if (err) {
			if (typeof err == "string") {
				res.status(400).json({
					message: err
				});
			} else {
				res.status(400).json(err);
			}
		} else if (utility.HTMLfilter(result) instanceof Error) {
			res.status(400).json(utility.HTMLfilter(result));
		} else {
			res.send({
				"draw": req.query.draw,
				"recordsTotal": result.length,
				"recordsFiltered": result.length,
				"data": utility.HTMLfilter(result),
				"updateId": update.Id
			});
		}
	})
})

router.post('/save', function(req, res) {
	delete req.body._csrf
	async.waterfall([
		(next) => service.setDataBulk(next, [req.body])
	], function(err, result) {
		if (err) {
			if (typeof err == "string") {
				res.status(400).json({
					message: err
				});
			} else {
				res.status(400).json(err);
			}
		} else {
			res.status(200).json('success');
		}
	})
});








router.get('/export', function(req, res) {

	var params = utility.getParams(req.query);
	async.waterfall([
		(next) => service.getData(next, params),
		(data, next) => service.convertToXLSX(next, data)
	], function(err, result) {
		if (err) {
			if (typeof err == "string") {
				res.status(400).json({
					message: err
				});
			} else {
				res.status(400).json(err);
			}
		} else {
			res.attachment('Answer_' + utility.dateTime2LocalString(utility.getCurrentHKTime(new Date)) + '.xlsx');
			res.status(200).send(result);
		}
	})
});

router.post('/import', multipartMiddleware, function(req, res) {
	var temp = update
	delete req.body._csrf
	async.waterfall([
		(next) => service.XLSXToArray(next, req.files.files.path),
		(faqs, next) => service.setDataBulk(next, faqs)
	], function(err, result) {
		if (err) {
			update = temp
			if (typeof err == "string") {
				res.status(400).json({
					message: err
				});
			} else {
				res.status(400).json(err);
			}
		} else {
			update = {
				User: undefined,
				Id: utility.generateId(10)
			}
			auditLog.insert('import', 'cms', 'Answer', null, result);
			res.status(200).json('Import Answer success');
		}
	})
})
/*
router.get('/:id', function(req, res) {
	console.log("view detail");
	var params = getParams(req.query);
	async.waterfall([
		function(next) {
			service.getData(function(err, data) {
				if (err) {
					throw err;
				}
				next(null, data);
			}, params);
		}
	], function(err, result) {
		if (err) {
			res.status(400).send(err);
		} else {
			res.send(result[0]);
		}
	})
})*/

function initialize(config) {
	if (typeof config !== 'object') {
		throw 'invalid config';
	}
	var AuditLog = require(config.cloudant.location).Audit
	auditLog = new AuditLog(config.cloudant);
	utility = require(config.utility);
	update.Id = utility.generateId(10);
}

function getViewPath(view) {
	return path.join(__dirname, '../views' + view);
}

exports = module.exports = router;
exports.initialize = initialize;
/*exports = module.exports = function(config) {
    console.log(config.cloudant);
    Object.assign(configuration, config);
    
    auditLog = new AuditLog(configuration.cloudant);
    return router;
};*/
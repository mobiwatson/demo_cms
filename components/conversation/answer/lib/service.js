var cloudantdb;

var _config = {
	server: {}
};

var xlsxExport = require('node-excel-export');
var xlsx = require('xlsx');
var async = require('async');
var utility;

/* Summary: search faq document with specified keyword in answer */
// callback(err, docs);
// criteria = { search: { EN: "str", TC: "str", SC: "str" }};
function getData(callback, criteria) {
	try {
		if (typeof callback !== 'function') {
			throw 'no callback'
		}

		var qs = utility.getQueryString(criteria);
		var docs;

		var db = cloudantdb(_config, callback);
		// if (typeof criteria.skip !== 'undefined' && criteria.skip !== null) { qs.skip = parseInt(criteria.skip); }
		// if (typeof criteria.take !== 'undefined' && criteria.take !== null) { qs.limit = parseInt(criteria.take); }

		// console.log(criteria);
		// console.log(JSON.stringify(qs));
		// if (typeof qs.selector["$or"] !== 'undefined') { console.log(qs.selector["$or"]); }
		db.find(qs, function(err, data) {
			if (err)
				callback(err);
			else {
				callback(null, data.docs);
			}
		});
	} catch (err) {
		callback(err, null);
	}
}

/* Summary: xlsx file to json array for import*/
// callback(err, body)
// excel file
function XLSXToArray(callback, path) {
	try {
		var header = {
			_id: "ans_id",
			intent: "Intent",
			TC: "TC",
			EN: "EN",
			startDate: "Start Date",
			EXPDate: "Exipry Date",
			EXP$TC: "Expired Answer TC",
			EXP$EN: "Expired Answer EN",
			fol_ids: "Follow Up Answer ID"
		};
		var match = {};
		var docs = xlsx.readFile(path);
		var ws = docs.Sheets[docs.Workbook.Sheets[0].name];
		var address = ws["!ref"];
		var maxCol = address.match(/A1:([A-Z]*)\d*/)[1];
		var maxRow = address.match(/A1:[A-Z]*(\d*)/)[1];
		for (var i = 0; i <= utility.ColumnToInt(maxCol); i++) {

			var column = utility.IntToColumn(i);
			if (!ws[column + "1"])
				continue
			Object.keys(header).forEach(_h => {
				if (header[_h] == ws[column + "1"].v.trim()) {
					if (match[_h] != undefined) {
						throw "Have Same Column: " + header[_h];
					} else {
						match[_h] = column;
					}
				}
			});
		}
		var notMatch = Object.keys(header).filter(_h => Object.keys(match).indexOf(_h) == -1);

		if (notMatch.length > 0) {
			throw notMatch.map(_h => header[_h]).join(",") + " not found";
		}
		var data = [];
		for (var i = 1; i <= maxRow; i++) {

			if (typeof ws[match._id + i] === "undefined" || ws[match._id + i].v.trim() == "") {
				continue;
			}

			if (i != 1) {

				var newData = {
					EXP: {}
				}
				Object.keys(match).forEach(_m => {
					if (ws[match[_m] + i] != undefined) {
						if (_m == "fol_ids") {
							newData[_m] = ws[match[_m] + i].w.split(",").filter(_d => _d.trim() != "");
						} else {
							utility.XLSXAssignData(newData, ws[match[_m] + i].w.trim(), _m.split('$'));
						}
					}
				})
				data.push(newData);
			}
		}
		callback(null, data);

	} catch (err) {
		callback(err, null);
	}
}

/* Summary: fill in revision ID for docs without it, then insert/ update DB with docs */
// callback(err, body)
// docs = [{ _id: "str", EN = "str", SC = "str", TC = "str" }]
function setDataBulk(callback, docs) {
	_fillInRev(function(err, docs) {
		if (err)
			callback(err, null);
		else {
			var db = cloudantdb(_config, callback);
			db.insert_bulk(docs, callback);
		}
	}, docs);
}

// callback(err);

function _fillInRev(callback, docs) {
	// put back rev id in case we havent
	async.waterfall([
		(next) => getData(next, {
			search: {
				_id: docs.map(_i => _i._id)
			}
		}),
	], (err, data) => {
		if (err) callback(err)
		else {
			docs.forEach(_d => {
				if (data.find(_data => _data._id === _d._id)) {
					if (_d._rev == undefined) {
						_d._rev = data.find(_data => _data._id === _d._id)._rev;
					}
					_d.in_use = data.find(_data => _data._id === _d._id).in_use;
				}
				_d.TC = _d.TC || []
				_d.EN = _d.EN || []
				_d.SC = _d.SC || []
			});
			callback(null, docs);
		}
	})
}

function convertToXLSX(callback, data) {
	try {
		var specification = {
			_id: { // <- the key should match the actual data key 
				displayName: 'ans_id',
				headerStyle: utility.xlsxFormat.headerRed,
				cellStyle: utility.xlsxFormat.cellRed,
				width: 120
			},
			modifiedDate: {
				displayName: "Modified Date",
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			category: {
				displayName: 'Category',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			intent: {
				displayName: 'Intent',
				headerStyle: utility.xlsxFormat.headerDark,
				cellStyle: utility.xlsxFormat.cellRed,
				width: 120
			},
			EN: {
				displayName: 'EN',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			TC: {
				displayName: 'TC',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			startDate: {
				displayName: 'Start Date',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			EXPDate: {
				displayName: 'Exipry Date',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			fol_ids: {
				displayName: 'Follow Up Answer ID',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			EXPTC: {
				displayName: 'Expired Answer TC',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			},
			EXPEN: {
				displayName: 'Expired Answer EN',
				headerStyle: utility.xlsxFormat.headerDark,
				width: 120
			}
		};

		data.forEach(_d => {
			_d.EXPTC = _d.EXP.TC;
			_d.EXPEN = _d.EXP.EN;
		});
		callback(null, xlsxExport.buildExport(
			[ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report 
				{
					name: 'Answer', // <- Specify sheet name (optional) 
					specification: specification, // <- Report specification 
					data: data // <-- Report data 
				}
			]
		));
	} catch (err) {
		callback(err, null);
	}
}

function initialize(config) {
	if (typeof config !== 'object') {
		throw 'Invalid config';
	}
	utility = require(config.utility);
	cloudantdb = require(config.cloudant.location);
	Object.assign(_config.server, config.cloudant);
	_config.dbname = config.database.faq;
}

exports = module.exports;
exports.initialize = initialize;
exports.getData = getData;
exports.setDataBulk = setDataBulk;
//exports.deleteDataBulk = deleteDataBulk;
exports.convertToXLSX = convertToXLSX;
exports.XLSXToArray = XLSXToArray;


module.exports = {
	moduleGroup: "conversation",
	item: [{
		name: "answer",
		module: require("./answer")
	}, {
		name: "intent",
		module: require("./intent")
	}]
};
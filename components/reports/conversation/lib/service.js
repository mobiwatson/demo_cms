var async = require('async');
var utility,
	cloudantdb;
var excel = require('node-excel-export');
var XLSX = require('xlsx');

var _config = {
	server: {}
};
var watsonConfig = {};
var workspaceConfig = {};
var watson;
var exporting = ["test", "test2"];


function getDataCount(callback, _criteria) {
	var criteria = JSON.parse(JSON.stringify(_criteria))
	if (typeof callback !== 'function') {
		throw 'no callback'
	}
	const searchQuery = utility.getSearchQuery(criteria);
	// const mapFunc = utility.getMapFunc(criteria);
	// console.log(mapFunc);
	const db = cloudantdb(_config);
	db.search('cms', 'filter', {
		q: searchQuery,
		include_docs: false
	}, (err, doc) => {
		if (err) callback(err)
		else {
			callback(null, doc.total_rows);
		}
	})
}

function getData(callback, criteria, count) {
	if (typeof callback !== 'function') {
		throw 'no callback'
	}
	var db = cloudantdb(_config, callback);
	// if (typeof qs.selector["$or"] !== 'undefined') { console.log(qs.selector["$or"]); }
	// get confidence level from watson
	// var qs = utility.getQueryString(criteria);
	// console.log('criteria', criteria);
	const params = utility.getSearchParams(criteria);
	params.include_docs = true;
	// console.log('params', params);


	if (!params.skip)
		params.skip = 0;
	db.advSearch('cms', 'filter', params, (err, body) => {
		if (err) callback(err);
		else callback(null, body.rows.map(row => {
				if (row.doc.output) {
					row.doc.output = JSON.stringify(row.doc.output)
				}
				if (row.doc.entity) {
					row.doc.entity = JSON.stringify(row.doc.entity)
				}
				return row.doc
			}));
	})
}

function stopExport(exportId) {
	var temp = exporting.findIndex(_e => _e == exportId)
	if (temp != -1)
		exporting.splice(temp, 1)
	console.log(exporting)
}

function getDataExcel(callback, criteria, total, res, exportId) {
	var db = cloudantdb(_config, callback);
	// if (typeof qs.selector["$or"] !== 'undefined') { console.log(qs.selector["$or"]); }
	// get confidence level from watson
	// var qs = utility.getQueryString(criteria);
	console.log('criteria', criteria);
	exporting.push(exportId)
	const params = utility.getQueryString(criteria);
	console.log(params.selector)
	var series = []
	var current = 0
	for (var i = 0; i < total / 10000; i++) {
		series.push((next) => {
			if (res)
				res.write('data: ' + JSON.stringify({
						current: current * 10000,
						total: total,
						status: "Getting Data",
					}) + '\n\n');
			var start = Date.now()
			db.find({
				selector: params.selector,
				limit: 10000,
				skip: current * 10000
			}, function(err, data) {
				console.log(Date.now() - start)
				if (!exporting.includes(exportId)) {
					next("Stopped by User")
				} else {
					current++;
					if (err) {
						next(err);
					} else if (data.error) {
						next(data.error);
					} else {
						var result = data.docs;
						result.forEach(function(d) {
							if (d.output) {
								d.output = JSON.stringify(d.output)
							}
							if (d.entity) {
								d.entity = JSON.stringify(d.entity)
							}
							if (d.datetime)
								d.datetime = utility.dateTime2LocalString(d.datetime);
						})
						next(err, result);
					}
				}
			});
		})
	}
	async.series(series, (err, result) => {
		console.log("checking")
		stopExport(exportId)
		if (err) callback(err)
		else callback(null, result.reduce((a, b) => a.concat(b)))
	})
}


function convertToExcel(callback, data) {
	var wb = XLSX.utils.book_new();
	var new_ws_name = "SheetJS";
	var ws_data = [
		["Session Id", "Device Id", "DateTime", "Input", "Conversation Type", "Language", "Intent 1", "Confidence Level 1", "Intent 2", "Confidence Level 2", 'Intent 3', 'Confidence Level 3', 'Intent 4', 'Confidence Level 4', 'Output', 'Answer Id', 'Answer Category']
	];
	data.forEach(convlog => {
		ws_data.push([
			convlog.session_id,
			convlog.device_id,
			utility.dateTime2LocalString(convlog.datetime),
			convlog.input === undefined ? "n/a" : (convlog.input === null) ? "?" : convlog.input,
			convlog.conversationType,
			convlog.language,
			convlog.intent1,
			convlog.confidence_level1,
			convlog.intent2,
			convlog.confidence_level2,
			convlog.intent3,
			convlog.confidence_level3,
			convlog.intent4,
			convlog.confidence_level4,
			convlog.output === undefined ? "n/a" : (convlog.output === null) ? "?" : JSON.stringify(convlog.output),
			convlog.answer_id,
			convlog.answer_category,

		])
	});
	// sorting entities
	var ws = XLSX.utils.aoa_to_sheet(ws_data);

	XLSX.utils.book_append_sheet(wb, ws, new_ws_name);
	callback(null, wb)
}


function initialize(config) {
	utility = require(config.utility);
	cloudantdb = require(config.cloudant.location);
	Object.assign(_config.server, config.cloudant);
	_config.dbname = config.database.convlog;
}

module.exports.initialize = initialize;
module.exports.getData = getData;
module.exports.getDataCount = getDataCount;
module.exports.convertToExcel = convertToExcel;
module.exports.getDataExcel = getDataExcel;
module.exports.stopExport = stopExport;



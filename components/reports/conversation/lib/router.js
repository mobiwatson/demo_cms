var express = require('express');
var path = require('path');
var utility
var service = require('./service');
var async = require('async');
var XLSX = require('xlsx');
var fs = require('fs')
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var simualtorUpdate = {
	User: undefined,
	Id: undefined
};


var router = express.Router();
var _config = {},
	parentView

// static files
router.get('/scripts/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
})
router.get('/styles/*', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', req.url));
});

// page and action

router.get('/', function(req, res) {
	res.locals.baseUrl = req.originalUrl;
	res.render(getViewPath('/index'), {
		csrfToken: req.csrfToken(),
	});
});

router.get('/list', function(req, res, next) {
	var start = new Date();
	var params = utility.getParams(req.query, true);

	if (params.skip >= 1000) {
		res.status(400).send("It is unavailable to show more than 1000 data now, please change your searching criteria or ordering");
	} else {
		async.parallel([
			(callback) => service.getData(callback, params),
			(callback) => service.getDataCount(callback, params)
		], (err, data) => {
			if (!res.headersSent) {
				if (err) {
					res.status(400).send(err);
				} else if (utility.HTMLfilter(data) instanceof Error) {
					res.status(400).json(utility.HTMLfilter(data[0]));
				} else {
					res.send({
						"draw": req.query.draw,
						"recordsTotal": data[1],
						"recordsFiltered": data[1],
						"data": utility.HTMLfilter(data[0])
					});
				}
			}
		})
	}

});

router.get('/export', function(req, res) {
	var params = utility.getParams(req.query);
	params.orders = [{
		"_id": "asc"
	}]
	console.log(params)
	var start = Date.now()
	var exportId = utility.generateId(10)
	res.writeHead(200, {
		"Content-Type": "text/event-stream",
		"Cache-Control": "no-store",
		"Access-Control-Allow-Origin": "*"
	});
	res.write('data: ' + JSON.stringify({
			status: "Initializing",
			exportId: exportId
		}) + '\n\n');
	var keepConnection = setInterval(() => {
		res.write('data: ' + JSON.stringify({
				status: "Continue"
			}) + '\n\n');
	}, 15 * 1000)
	async.waterfall([
		(next) => service.getDataCount(next, params),
		(data, next) => service.getDataExcel(next, params, data, res, exportId),
		(data, next) => service.convertToExcel(next, data),
		(data, next) => {
			res.write('data: ' + JSON.stringify({
					status: "Generating File",
				}) + '\n\n');
			var fileName = '/download/conversation_log_' + utility.dateTime2LocalString(utility.getCurrentHKTime(new Date)).replace(/[:-]/g, "") + '.xlsx'
			console.log(fileName)
			XLSX.writeFile(data, "./public" + fileName);
			setTimeout(() => {
				console.log('delete file')
				fs.unlink("./public" + fileName, (err) => {
					if (err)
						throw err;
				});
			}, 24 * 60 * 60 * 1000)
			next(null, fileName)
		}
	], (err, data) => {
		console.log(start - Date.now())
		if (err)
			res.write('data: ' + JSON.stringify({
					status: "Error",
					data: err
				}) + '\n\n');
		else {
			res.write('data: ' + JSON.stringify({
					status: "Finished",
					data: data
				}) + '\n\n');
		}
		clearInterval(keepConnection);
		res.end()
	})
});

router.get('/stop', function(req, res) {
	service.stopExport(req.query.Id)
	res.status(200).send()
})

function getViewPath(view) {
	return path.join(__dirname, '../views/' + view);
}


function initialize(config) {
	if (typeof config !== 'object') {
		throw 'invalid config';
	}
	utility = require(config.utility);
	simualtorUpdate.Id = utility.generateId(10);
}

exports = module.exports = router;
exports.initialize = initialize;

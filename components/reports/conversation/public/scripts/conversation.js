var count = 0;
var exporting = false;
var currentParam = {};
var table;
var numberOfEntities;
var processBar
var exportId
function getColumns() {
	var columns = [
		{
			'targets': 0,
			'searchable': false,
			'orderable': false,
			'className': 'checkbox',
			'render': function(data, type, full, meta) {
				return '<input type="checkbox" data-id="' + full._id + '">';
			}
		},
		{
			'targets': 1,
			'searchable': false,
			'orderable': false,
			'render': function(data, type, full, meta) {
				// return '<a href="' + document.URL + '/' + full._id + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
				return '<a href="#" role="button" data-toggle="modal" data-target="#detailmodal" data-row="' + meta.row + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
			}
		},
		{
			"data": "datetime",
			"type": "dateTime",
		},
		
		{
			"data": "input",
			"type": "string",
			"render": function(value) {
				if (value === undefined) {
					return 'n/a';
				}
				if (value === null) {
					return "?";
				}
				return value;
			}
		}
	]
	columns = columns.concat([
		{
			"data": "intent1",
			"type": "string",
			"render": function(value) {
				if (value === undefined) {
					return "n/a";
				}
				if (value === null) {
					return "?";
				}
				return value;
			}
		},
		{
			"data": "confidence_level1",
			"type": "double",
			"render": function(value) {
				if (value === undefined) {
					return 'n/a';
				}
				if (value === null) {
					return "?";
				}
				return value;
			}
		}])

	columns = columns.concat([
		{
			"data": "language",
			"type": "string",
			"render": function(value) {
				if (value === undefined || value === null) {
					return "";
				}
				return value;
			}
		}
	
	])

	return columns.concat([
		{
			"data": "session_id",
			"type": "string",
			"render": function(value) {
				if (value === undefined) {
					return 'n/a';
				}
				if (value === null) {
					return "?";
				}
				return value.length > 10 ? value.substring(0, 20) + '...' : value;
			}
		}
	])
}

$(document).ready(function() {
	moment().format();

	//$("#exportMenu").hide()

	if (typeof $.datetimepicker !== 'undefined') {
		$('#sfromdate').datetimepicker({
			format: 'Y-m-d H:00:00'
		});
		$('#stodate').datetimepicker({
			format: 'Y-m-d H:00:00'
		});
	}
	$('#sfromdate').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});
	$('#stodate').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});
	$('#slang').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});
	$('#sintent').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});
	$('#sfromconflv').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});
	$('#stoconflv').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});
	$('#sinput').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});

	$('#ssessionid').on('keypress', function(e) {
		if (e.which === 13) {
			$('#search').click()
		}
	});

	

	$('#search').click(function(e) {
		e.preventDefault()
		table.draw();
	});

	$('#clear').click(function(e) {
		$('#sfromdate').val("");
		$('#stodate').val("");
		$('#slang').val("");
		$('#sintent').val("");
		$('#sfromconflv').val("");
		$('#stoconflv').val("");
		$('#sinput').val("");
		$('#ssessionid').val("");
		$('#sdeviceid').val("");
		$('#sconversationtype').val("");
	});

	// prevent frequent request in front-end
	var preDrawCallback = function(setting) {
		if (count > 1) {
			return false;
		}
		count++;
		return true;
	};

	// export selected row to csv file
	var exportData = function(e, dt, node, config, selected) {
		$('#cancelDownload').hide()
		$('#exportMenu').show().css('display', 'flex')

		var reqParam = currentParam;
		delete reqParam.start;
		delete reqParam.length;

		if (selected) {
			var selected = [];
			$.each($('tbody input:checked'), function(index, value) {
				selected.push($(value).data('id'));
			});
			if (selected.length > 0) {
				var source = new EventSource(window.location.pathname + '/export?selected=' + selected.join(","));
			} else {

				setTimeout(function() {
					$('#exportMenu').hide()
				}, 1000)
				alert("Please select as least one row")
				return
			}
		} else {
			var source = new EventSource(window.location.pathname + '/export?' + $.param(reqParam));

		}
		$('#cancelDownload').click(function(e) {
			source.close()
			$.ajax({
				url: window.location.pathname + '/stop?Id=' + exportId,
			})
			setTimeout(function() {
				$('#exportMenu').hide()
			}, 1000)
		})
		source.addEventListener('message', function(e) {
			var jsonData = JSON.parse(e.data);
			console.log(jsonData)
			if (jsonData.status != "Continue") {
				if (jsonData.status == "Initializing") {
					exportId = jsonData.exportId
					$('#progressStatus').text(jsonData.status)
					$('#cancelDownload').show()
				} else if (jsonData.status == "Getting Data") {
					$('#progressStatus').text(jsonData.status + " " + Math.floor((jsonData.current || 0) / (jsonData.total || 1) * 100) + "%" + ' (' + (jsonData.current || 0) + '/' + (jsonData.total || 1) + ")")
					$('#progressBar')[0].style.width = (jsonData.current || 0) / (jsonData.total || 1) * 100 + "%"
				} else if (jsonData.status == "Finished") {
					$('#progressStatus').text(jsonData.status)
					source.close();
					setTimeout(function() {
						$('#exportMenu').hide()
						$('#progressBar')[0].style.width = "0%"
					}, 1000)
					var link = document.createElement('a');
					link.href = jsonData.data;
					link.click();
				} else if (jsonData.status == "Error") {
					source.close()
					$('#progressStatus').text(jsonData.status + "\n" + jsonData.data)
					setTimeout(function() {
						$('#exportMenu').hide()
					}, 1000)
				} else {
					$('#progressStatus').text(jsonData.status)
					$('#progressBar')[0].style.width = "100%"
				}
			}
		//alert("My message: " + jsonData.status);
		});
		source.addEventListener('error', function(e) {
			source.close()
			setTimeout(function() {
				$('#exportMenu').hide()
			}, 1000)
			console.log()
			alert(e.data)
		});
	};
	table = $('#cvtable').DataTable({
		"serverSide": true,
		"searching": false,
		"processing": false,
		"dom": 'B<"row"<"col-sm-6 paging"l><"col-sm-6 moreinfo"i>>frtp',
		"buttons": [
			{
				"text": 'Export selected',
				"action": function(e, dt, node, config) {
					exportData(e, dt, node, config, true);
				}
			},
			{
				"text": "Export All",
				"action": function(e, dt, node, config) {
					exportData(e, dt, node, config);
				}
			}
		],
		"ajax": {
			"url": window.location.pathname + '/list',
			"paging": false,
			"error": function(xhr, error, thrown) {
				if (xhr.status === 0 || xhr.readyState === 0) {
					return;
				}
				alert(xhr.responseText);
			},
			"data": function(d) {
				showLoading()
				d.count = numberOfEntities;
				d.cs = {};

				if ($('#sfromdate').val() !== null && $('#sfromdate').val() !== "") {
					if (d.cs.datetime == undefined)
						d.cs.datetime = {};
					d.cs.datetime.from = $('#sfromdate').val()
				}
				if ($('#stodate').val() !== null && $('#stodate').val() !== "") {
					if (d.cs.datetime == undefined)
						d.cs.datetime = {};
					d.cs.datetime.to = $('#stodate').val()
				}

				if ($('#slang').val() !== null && $('#slang').val() !== "") {
					d.cs.language = $('#slang').val()
				}
				if ($('#sintent').val() !== null && $('#sintent').val() !== "") {
					d.cs.intent1 = $('#sintent').val();
				}
				if ($('#sfromconflv').val() !== null && $('#sfromconflv').val() !== "") {
					if (d.cs.confidence_level1 == undefined)
						d.cs.confidence_level1 = {};
					d.cs.confidence_level1.from = $('#sfromconflv').val();
				}
				if ($('#stoconflv').val() !== null && $('#stoconflv').val() !== "") {
					if (d.cs.confidence_level1 == undefined)
						d.cs.confidence_level1 = {};
					d.cs.confidence_level1.to = $('#stoconflv').val();
				}
				if ($('#sinput').val() !== null && $('#sinput').val() !== "") {
					d.cs.input = $('#sinput').val().replace(/</, "$lt;").replace(/>/, "$gt;");
				}
				
				if ($('#ssessionid').val() !== null && $('#ssessionid').val() !== "") {
					d.cs.session_id = $('#ssessionid').val();
				}
				
				currentParam = d;

				return d;
			},
			"dataSrc": function(json) {
				return json.data;
			},
			"complete": function(jqXHR, textStatus) {
				count = 0;
				if ($('#select-all:checked').length > 0) {
					$('#select-all:checked').click();
				}
				hideLoading()
			}
		},
		"order": [
			[2, "desc"]
		],
		"columns": getColumns()
	});
	$('#select-all').on('click', function() {
		// Check/uncheck all checkboxes in the table
		var rows = table.rows({
			'search': 'applied'
		}).nodes();
		$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});

	$('#cvtable tbody').on('change', 'input[type="checkbox"]', function() {
		// If checkbox is not checked
		if (!this.checked) {
			var el = $('#select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if (el && el.checked && ('indeterminate' in el)) {
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
		}
	});
	$('#detailmodal').on('show.bs.modal', function(event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var row = button.data('row');

		var data = table.row(row).data();
		// Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		// var modal = $(this)
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body input').val(recipient)

		$("#fdateTime").text(decode(data.datetime));
		$("#fsessionID").text(decode(data.session_id));
		$("#flang").text(decode(data.language));
		$("#finput").text(decode(data.input));
		$("#foutput").text(decode(data.output));
		$("#fIntent1").text(decode(data.intent1));
		$("#fconfLV1").text(decode(data.confidence_level1));
		$("#fIntent2").text(decode(data.intent2));
		$("#fconfLV2").text(decode(data.confidence_level2));
		$("#fIntent3").text(decode(data.intent3));
		$("#fconfLV3").text(decode(data.confidence_level3));
		$("#fIntent4").text(decode(data.intent4));
		$("#fconfLV4").text(decode(data.confidence_level4));
		$("#fIntent5").text(data.intent5);
		$("#fconfLV5").text(data.confidence_level5);
		$("#fentity").text(decode(data.entity));

	});
	$('#detailmodal').on('hide.bs.modal', function(event) {
		$("#detailmodal input").val("");
		$("#detailmodal textarea").val("");
	});
})
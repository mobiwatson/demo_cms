'use strict';

var menu = require('./lib/menu');
var router = require('./lib/router');
var service = require('./lib/service')
// var conversation = new watson.ConversationV1()

function initialize(config) {
	router.initialize(config);
    service.initialize(config);
}

exports.initialize = initialize;
exports.router = router;
exports.menu = menu;

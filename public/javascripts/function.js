function getAbsoluteLeft(objectId) {
	o = document.getElementById(objectId)
	oLeft = o.offsetLeft
	while(o.offsetParent!=null) {
		 oParent = o.offsetParent 
		 oLeft += oParent.offsetLeft
		 o = oParent
	}
	return oLeft
   }
   
   
   
   function getAbsoluteRight(objectId) {
	o = document.getElementById(objectId)
	w = o.offsetWidth
	oLeft = o.offsetLeft
	while(o.offsetParent!=null) {
		 oParent = o.offsetParent 
		 oLeft += oParent.offsetLeft
		 o = oParent
	}
	return oLeft + w
   }
   
   function getAbsoluteTop(objectId) {
	o = document.getElementById(objectId)
	oTop = o.offsetTop
	while(o.offsetParent!=null) {
		 oParent = o.offsetParent 
		 oTop += oParent.offsetTop
		 o = oParent
	}
	return oTop
   }
   
   function getAbsoluteBottom(objectId) {
	o = document.getElementById(objectId)
	h = o.offsetHeight;
	oTop = o.offsetTop
	while(o.offsetParent!=null) {
		 oParent = o.offsetParent 
		 oTop += oParent.offsetTop
		 o = oParent
	}
	return oTop + h
   }
   
   
   
   PositionX = 50;
   PositionY = 50;
   defaultWidth  = 500;
   defaultHeight = 500;
   
   var AutoClose = true;
   if (parseInt(navigator.appVersion.charAt(0))>=4){
	   var isNN=(navigator.appName=="Netscape")?1:0;
	   var isIE=(navigator.appName.indexOf("Microsoft")!=-1)?1:0;
   }
   
   var optNN='scrollbars=no,width='+defaultWidth+',height='+defaultHeight+',left='+PositionX+',top='+PositionY;
   var optIE='scrollbars=no,width=150,height=100,left='+PositionX+',top='+PositionY;
   
   function popImage(imageURL,imageTitle){
	   if (isNN)
		   imgWin=window.open('about:blank','',optNN);
   
	   if (isIE)
		   imgWin=window.open('about:blank','',optIE);
   
	   with (imgWin.document){
		   writeln('<html><head><title>Loading...</title><style>body{margin:0px;}</style>');writeln('<sc'+'ript>');
		   writeln('var isNN,isIE;');writeln('if (parseInt(navigator.appVersion.charAt(0))>=4){');
		   writeln('isNN=(navigator.appName=="Netscape")?1:0;');writeln('isIE=(navigator.appName.indexOf("Microsoft")!=-1)?1:0;}');
		   writeln('function reSizeToImage(){');writeln('if (isIE){');writeln('window.resizeTo(100,100);');
		   writeln('width=100-(document.body.clientWidth-document.images[0].width);');
		   writeln('height=100-(document.body.clientHeight-document.images[0].height);');
		   writeln('window.resizeTo(width,height);}');writeln('if (isNN){');       
		   writeln('window.innerWidth=document.images["George"].width;');writeln('window.innerHeight=document.images["George"].height;}}');
		   writeln('function doTitle(){document.title="'+imageTitle+'";}');writeln('</sc'+'ript>');
		   if (!AutoClose)
			   writeln('</head><body oncontextmenu="return false;" bgcolor=000000 scroll="no" onload="reSizeToImage();doTitle();self.focus()">');
		   else
			   writeln('</head><body oncontextmenu="return false;" bgcolor=000000 scroll="no" onload="reSizeToImage();doTitle();self.focus()" onblur="self.close()">');
   
		   writeln('<img name="George" src='+imageURL+' style="display:block" GALLERYIMG="no"></body></html>');
		   close();
	   }
   }
   
   
   
   function checkNo(txt){
	   validChars = "1234567890-+()#* ";
	   
	   for (i=0; i < txt.length; i++) {
		   txtChar = txt.charAt(i);
		   if (validChars.indexOf(txtChar, 0) == -1) {
			   return false;
		   }
	   }
	   
	   return true;
   }
   
   
   function isYear(txt){
	   validChars = "1234567890";
	   
	   if (txt.length != 4){
		   return false;	
	   }
	   
	   for (i=0; i < txt.length; i++) {
		   txtChar = txt.charAt(i);
		   if (validChars.indexOf(txtChar, 0) == -1) {
			   return false;
		   }
	   }
	   
	   return true;
   }
   
   
   function isPwd(txt){
	   validChars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	   
	   for (i=0; i < txt.length; i++) {
		   txtChar = txt.charAt(i);
		   if (validChars.indexOf(txtChar, 0) == -1) {
			   return false;
		   }
	   }
	   
	   return true;
   }
   
   
   function checkPrice(txt){
	   validChars = "1234567890.";
	   
	   for (i=0; i < txt.length; i++) {
		   txtChar = txt.charAt(i);
		   if (validChars.indexOf(txtChar, 0) == -1) {
			   return false;
		   }
	   }
	   
	   return true;
   }
   
   
   function isNumeric(txt){
	   validChars = "1234567890";
	   
	   for (i=0; i < txt.length; i++) {
		   txtChar = txt.charAt(i);
		   if (validChars.indexOf(txtChar, 0) == -1) {
			   return false;
		   }
	   }
	   
	   return true;
   }
   
   
   function MM_preloadImages() { //v3.0
	 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
   }
   
   function MM_swapImgRestore() { //v3.0
	 var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
   }
   
   function MM_findObj(n, d) { //v4.01
	 var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	   d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	 if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	 for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	 if(!x && d.getElementById) x=d.getElementById(n); return x;
   }
   
   function MM_swapImage() { //v3.0
	 var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	  if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
   }
   
   function LTrim(str){
	  var whitespace = new String(" \t\n\r");
   
	  var s = new String(str);
   
	  if (whitespace.indexOf(s.charAt(0)) != -1) {
		 var j=0, i = s.length;
		 while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
			j++;
		
		 s = s.substring(j, i);
	  }
	  return s;
   }
   
   function RTrim(str){
	  var whitespace = new String(" \t\n\r");
   
	  var s = new String(str);
   
	  if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
		 var i = s.length - 1;       // Get length of string
		 while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
			i--;
		 s = s.substring(0, i+1);
	  }
	  return s;
   }
   
   function Trim(str){
	  return RTrim(LTrim(str));
   }
   
   function isEmail(string) {
	   if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
		   return true;
	   else
		   return false;
   }
   
   
   Now = new Date();
   NowDay = Now.getDate();
   NowMonth = Now.getMonth();
   NowYear = Now.getYear();
   if (NowYear < 2000) NowYear += 1900; //for Netscape
   
   //function for returning how many days there are in a month including leap years
   function DaysInMonth(WhichMonth, WhichYear)
   {
	 var DaysInMonth = 31;
	 if (WhichMonth == "4" || WhichMonth == "6" || WhichMonth == "9" || WhichMonth == "11") DaysInMonth = 30;
	 if (WhichMonth == "2")
	 {
		 if ((WhichYear % 4 == 0 && WhichYear % 100 != 0) || WhichYear % 400 == 0 || WhichYear == 0) 
			 DaysInMonth = 29;
		 else
			 DaysInMonth = 28;
	 }
	 return DaysInMonth;
   }
   
   //function to change the available days in a months
   function ChangeOptionDays(Which)
   {
	 DaysObject = eval("document.myform." + Which + "Day");
	 MonthObject = eval("document.myform." + Which + "Month");
	 YearObject = eval("document.myform." + Which + "Year");
   
	 Month = MonthObject[MonthObject.selectedIndex].text;
	 Year = YearObject.value;
   
	 DaysForThisSelection = DaysInMonth(Month, Year);
	 CurrentDaysInSelection = DaysObject.length;
	 if (CurrentDaysInSelection > DaysForThisSelection)
	 {
	   for (i=0; i<(CurrentDaysInSelection-DaysForThisSelection); i++)
	   {
		 DaysObject.options[DaysObject.options.length - 1] = null
	   }
	 }
	 if (DaysForThisSelection > CurrentDaysInSelection)
	 {
	   for (i=0; i<(DaysForThisSelection-CurrentDaysInSelection); i++)
	   {
		 NewOption = new Option(DaysObject.options.length + 1, DaysObject.options.length + 1);
		 DaysObject.add(NewOption);
	   }
	 }
	   if (DaysObject.selectedIndex < 0) DaysObject.selectedIndex == 0;
   }
   
   //function to set options to today
   function SetToToday(Which)
   {
	 DaysObject = eval("document.myform." + Which + "Day");
	 MonthObject = eval("document.myform." + Which + "Month");
	 YearObject = eval("document.myform." + Which + "Year");
   
   //  YearObject[0].selected = true;
	 MonthObject[NowMonth].selected = true;
   
	 ChangeOptionDays(Which);
   
	 DaysObject[NowDay-1].selected = true;
   }
   
   //function to write option years plus x
   function WriteYearOptions(YearsAhead)
   {
	 line = "";
	 for (i=0; i<YearsAhead; i++)
	 {
	   line += "<OPTION>";
	   line += NowYear + i;
	 }
	 return line;
   }
   
   
   
   function showImg(img){
	   img = img.replace(".jpg", "_small.jpg");
	   document.writeln(" <img src=\"../products/" + img + "\" border=0 ");
   }
   
   
   function isPositiveInt(obj_id){
	   myvalue = document.getElementById(obj_id).value;
	   if(myvalue.search(/^[0-9]+$/) != -1){
		   return true;
	   }else{
		   return false;
	   }
   }
   
   function isPositiveDec(obj_id){
	   myvalue = document.getElementById(obj_id).value;
	   if(isPositiveInt(obj_id)){
		   return true;
	   }else if(myvalue.search(/^[0-9]+\.[0-9]+$/) != -1){
		   return true;
	   }else{
		   return false;
	   }
   }
   
   function popwin(path,mywidth, myheight,enable_resize,display_name){
	   var mywin = window.open(path ,display_name,'width=' + mywidth +', height=' + myheight +',location=no,directories=no,menubar=no,toolbar=no,scrollbars=yes,status=no,resizable='+enable_resize+',left=0,top=0');
	   mywin.focus();
   }
   
   
   function Checkphone(obj_id,ptype){
	   limit_char="0123456789";
	   if(ptype=="normal")
		   limit_init="235689";
	   else if(ptype=="mobile")
	   limit_init="235689";	
	   check= true;
	   var no = document.getElementById(obj_id).value;
	   
	   for (i=0;i<no.length;i++){
		   ch = no.charAt(i);
		   if(i<1){
			   if (limit_init.indexOf(ch)==-1){   
				   check=false;
				   break;
			   }
		   }
		   if (limit_char.indexOf(ch)==-1){   
			   check=false;
			   break;
		   }
	   }
	   
	   if(no.length < 8){
		   check=false;	
	   }
		if(no.length > 18){
		   check=false;	
	   }
	   return check;
   }   
   
   function check_radio(objname){
	   
	   rarr = document.getElementsByName(objname);	
	   Selection_value = "";
	   var chk = false;
	   var rlen = rarr.length;
	   for (i=0; i < rlen; i++) {
		   if (rarr[i].checked) {
			   chk  = true;
			   break;
		   }
	   }
	   return chk;
   }
   
   
   function unset_radio(objname){
	   
	   rarr = document.getElementsByName(objname);	
	   var rlen = rarr.length;
	   for (i=0; i < rlen; i++) {
			rarr[i].checked = false;
	   }
   }
   
   
   function get_radio_selected(objname){
	   
	   rarr = document.getElementsByName(objname);	
	   var Selection_value = "";
	   var rlen = rarr.length;
	   for (i=0; i < rlen; i++) {
		   if (rarr[i].checked) {		
			   Selection_value = rarr[i].value;
			   break;
		   }
	   }
	   return Selection_value;
   }
   function getFileExtension(filename) {   
	   var fileinput = document.getElementById(filename);   
	   if (!fileinput)     return "";   
	   var filename = fileinput.value;   
	   if (filename.length == 0)     return "";   
	   var dot = filename.lastIndexOf(".");   
	   if (dot == -1)     return "";   
	   var extension = filename.substr(dot, filename.length);   
	   return extension.toLowerCase(); 
   } 
   
   function is_checkbox_text(obj_name,intext){
   var obj_ = document.getElementsByName(obj_name);
		   var len = obj_.length;
		   
		   var checked_val = false;
	
		   for (i = 0; i < len; i++)
		   {
			   if (obj_[i].checked == true)
			   {
				  
				   if(intext == obj_[i].value){
					   //alert(obj_[i].value);
					   checked_val = true;
					   break;
					   }
				   
			   }
		   }
	   
	  return checked_val;
   }//
   
   function checkbox_count(obj_name){
   
	   
	   var obj_name =document.getElementsByName(obj_name);
		   var len_chk = obj_name.length;
				   
		   var chked_cnt = 0;
		   for (i = 0; i < len_chk; i++)
		   {
			   if (obj_name[i].checked == true)
			   {
				   chked_cnt++;
			   }
		   }//end loop
	//  alert(chked_cnt)
	  return chked_cnt;
	   
   }//end checkbox count
   
   function goBack()
	  {
	  window.history.back()
	 }
	 
	 
	 function popwin_re(myname, mywidth, myheight){
	   var mywin = window.open(myname ,'membercheckno','width=' + mywidth +', height=' + myheight +',location=no,directories=no,menubar=no,toolbar=no,scrollbars=yes,status=no,resizable=yes,left=0,top=0');
	   mywin.focus();
   }
   
   
   function load_link(ilink){
   
	   window.location = ilink;
   }
   
   
   function limitText(limitField, limitCount, limitNum) {
	   if (limitField.value.length > limitNum) {
		   limitField.value = limitField.value.substring(0, limitNum);
	   } else {
		   limitCount.value = limitNum - limitField.value.length;
	   }
   }
   
   function validFileExt(file) {
	   var extension = file.substr( (file.lastIndexOf('.') +1) );
	   switch(extension) {
		   case 'jpg':
			   return true;
		   break;
		   case 'png':
		   case 'gif':
		   case 'zip':
		   case 'rar':
		   case 'pdf':
			return false;
		   break;
		   default:return true;break;
	   }
   };
   function get_checkbox_selected(obj_name){
   var obj_ = document.getElementsByName(obj_name);
		   var len = obj_.length;
		   
		   var checked_val = "";
	
		   for (i = 0; i < len; i++)
		   {
			   if (obj_[i].checked == true)
			   {
				   checked_val+= (checked_val!="")?",":"";
				   checked_val+= obj_[i].value;
			   }
		   }
	   
	  return checked_val;
   }//
   
   function iniframe(){
		   var isInIframe = (window.location != window.parent.location) ? true : false;
		   return isInIframe;
	   }
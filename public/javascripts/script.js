function decode(encodedString) {
	var textArea = document.createElement('textarea');
	textArea.innerHTML = encodedString;
	return textArea.value;
}

function dateTime2LocalString(datetime, unknownDisp) {
	if (!datetime) {
		return unknownDisp || "n/a";
	}

	var dt = null;
	if (typeof datetime === "string") {
		dt = new Date(datetime);
	} else if (Object.prototype.toString.call(datetime) === '[object Date]') {
		dt = datetime;
	} else {
		throw new Error('Not support type: ' + Object.prototype.toString.call(datetime));
	}
	return dt.getFullYear().toString() + '-' +
		("0" + (dt.getMonth() + 1)).slice(-2) + '-' +
		("0" + dt.getDate()).slice(-2) + ' ' +
		("0" + dt.getHours()).slice(-2) + ':' +
		"00:00"
}

function getErrorMessage(jqXHR) {
	if (jqXHR.responseJSON) {
		if (jqXHR.responseJSON.error) {
			return jqXHR.responseJSON.error.message;
		} else if (jqXHR.responseJSON.message) {
			return jqXHR.responseJSON.message;
		} else {
			return jqXHR.responseText;
		}
	} else {
		return null;
	}
}
function getLoadingElement() {
	return $('<div style="text-align:center;"><div class="fa fa-spinner fa-spin" style="font-size:120px"></div><div><p name="status"></p><p name="progress"></p></div></div>');
}

function showFullPageLoading(text, options) {
	var opt = {
		image: "",
		custom: getLoadingElement(),
	};
	$.LoadingOverlay("show", opt);
	$("p[name=status]").text(text || "");
}


function hideFullPageLoading(text, options) {
	$.LoadingOverlay("hide", true);
}

function showLoading(text, options) {
	var opt = {
		image: "",
		custom: getLoadingElement(),
	};
	$(".main_content").LoadingOverlay("show", opt);
	$("p[name=status]").text(text || "");
}

function hideLoading() {
	$(".main_content").LoadingOverlay("hide", true);
}
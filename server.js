var app = require('./app');
var https = require('https');
var http = require('http');

var normalizePort = require('normalize-port');
var fs = require('fs');
var portHttp = normalizePort(process.env.PORT || '5056');
//var portHttps = normalizePort(process.env.PORTHTTPS || '5034');


app.set('port', portHttp);
//app.set('port',portHttps);
/*const privateKey = fs.readFileSync('/etc/pki/tls/private/151.key', 'utf8');
const certificate = fs.readFileSync('/etc/pki/tls/certs/151.crt', 'utf8');
const intermediate_cert = fs.readFileSync('/etc/pki/tls/certs/intermediate.crt', 'utf8');
const serverOption = {
    key: privateKey,
    cert: certificate,
    ca: intermediate_cert,
    passphrase: 'P@ssw0rd'
};
var serverhttps = https.createServer(serverOption,app)
    .listen(portHttps, '0.0.0.0', function() {
        console.log('Server running at port ' + portHttps);
    });
*/
var serverhttp = http.createServer(app)
	.listen(portHttp, '0.0.0.0', function() {
		console.log('Server running at port ' + portHttp);
	});



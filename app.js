const result = require('dotenv').config();
if (result.error) {
	throw result.error;
}

var express = require('express');
var timeout = require('connect-timeout');

var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var helmet = require('helmet');
var path = require('path');
var logger = require('morgan');
var contextService = require('request-context');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var utility = require(path.join(__dirname, process.env.UTILITY));
var account;

var app = express();
var csrf = require('csurf');
// get config

const configuration = {
	requireAudit: process.env.AUDIT,
	cloudant: {
		location: path.join(__dirname, process.env.CLOUDANTDB),
		account: process.env.CLOUDANT_ACCOUNT,
		password: process.env.CLOUDANT_PASSWORD
	},
	watson: {
		location: path.join(__dirname, process.env.CONV),
		username: process.env.WATSON_USERNAME,
		password: process.env.WATSON_PASSWORD
	},
	workspace: process.env.WATSON_WORKSPACE.split(","),
	chatbotLink: process.env.CHATBOT_LINK,
	database: {},
	workspaceLocation: {
		en: process.env.WATSON_WORKSPACE_EN,
		tc: process.env.WATSON_WORKSPACE_TC,
		sc: process.env.WATSON_WORKSPACE_SC
	},
	utility: path.join(__dirname, process.env.UTILITY)
};

//conf for component
process.env.CLOUDANT_DB.split(",").forEach((ele) => {
	configuration.database[ele.split(":")[0]] = ele.split(":")[1];
});

// view engine setup
app.set('views', [
	path.join(__dirname, 'views'),
	path.join(__dirname, 'component'),
//path.join(__dirname, process.env.ACCOUNT)
]);

app.set('view engine', 'jade');
app.set('view options', {
	layout: 'layout'
});

app.use(logger('dev'));

// Initialize sessions
app.use(bodyParser.json({
	parameterLimit: '10000'
}));

app.use(bodyParser.urlencoded({
	extended: true,
	parameterLimit: '10000'
}));

app.use(helmet());
app.disable('x-powered-by')
app.set('trust proxy', 1)

app.use(cookieParser());
app.use(cookieSession({
	name: 'session',
	keys: ['key1', 'key2'],
	httpOnly: true,
}));


// static routing
app.use(express.static(path.join(__dirname, 'public')));
app.use(contextService.middleware('request'));

// basic routing
app.use(timeout(30000));
app.use(function(req, res, next) {
	if (!req.timedout) next();
});

// pass layout setting to all pages
app.use(function(req, res, next) {

	if (utility.isRequestWebPage(req)) {
		res.locals.title = process.env.TITLE;
		res.locals.subtitle = process.env.SUBTITLE;
	}
	if (req.url.substr(-1) == '/' && req.url.length > 1) {
		res.redirect(301, req.url.slice(0, -1));
	} else {

		next();
	}
});
app.use(multipartMiddleware, function(req, res, next) {
	next()
})

let defaultMenu = [];
const modules = [];
// no need auth
const nomarlPage = ["/home"]
// add logout in menu if attached login module


function formatMenu(menu, url) {
	var result = [];
	menu.forEach(_m => {
		_m.selected = (_m.action) === url;
		if (_m.group !== undefined) {
			var groupIndex = result.findIndex(_res => _res.group == _m.group)
			if (groupIndex == -1)
				result.push(
					{
						group: _m.group,
						items: [_m],
						selected: _m.selected,
					}
				);
			else {
				result[groupIndex].items.push(_m);
				if (_m.selected)
					result[groupIndex].selected = _m.selected
			}
		} else {
			result.push(_m)
		}
	});
	return result
}

//pass menu and check access right

// initialize modules
if (process.env.MODULES !== undefined) {
	process.env.MODULES.split(',').forEach((m) => {
		try {
			var obj = require(`./components/${m}`);
			obj.item.forEach(_i => {
				modules.push({
					moduleGroup: obj.moduleGroup,
					name: _i.name,
					module: _i.module
				});
			})
			console.log(`Module ${m} attached`);
		} catch (err) {
			console.log(`Module ${m} cannot be attached.`);
			throw err;
		}
	});
}

// modules router and menu
// const login = modules.find(_m => _m.name == process.env.LOGIN);
// const rolesModules = modules.find(_m => _m.name == process.env.ROLES);
// const backupRestore = modules.find(_m => _m.name == process.env.BACKUPRESTORE);
// if (login) {
// 	var loginConfig = {
// 		type: process.env.LOGIN_TYPE,
// 		realm: process.env.LOGIN_REALM,
// 		identityProviderUrl: process.env.LOGIN_URL,
// 		thumbprint: process.env.LOGIN_THUMBPRINT
// 	}
// 	if (rolesModules) {
// 		loginConfig.authendication = rolesModules.module.roles.getRole
// 	}
// 	login.module.initialize(configuration, loginConfig);
// 	app.use(login.module.router);
// 	console.log('router initialize ' + login.name);
// }

// app.use(function(req, res, next) {
// 	if (req.user) {
// 		contextService.set('request:user', {
// 			username: req.user.username,
// 			userGroup: req.user.userGroup,
// 			homePage: req.user.homePage
// 		});
// 		contextService.set('request:info', {
// 			ip: req.connection.remoteAddress,
// 			userGroup: req.user.userGroup
// 		});
// 	} 
// 	next();
// });


app.use(csrf({
	cookie: true
}));
app.use(function(err, req, res, next) {
	if (err.code !== 'EBADCSRFTOKEN') {
		return next(err)
	} else {

		next(err);
	}
// handle CSRF token errors here 
})
app.use(function(req, res, next) {
	if (utility.HTMLfilter(req.body) instanceof Error) {
		const err = utility.HTMLfilter(req.body);
		err.status = 400;
		next(err);
	} else if (utility.HTMLfilter(req.query) instanceof Error) {
		const err = utility.HTMLfilter(req.query);
		err.status = 400;
		next(err);
	} else {
		req.body = utility.HTMLfilter(req.body);
		req.query = utility.HTMLfilter(req.query)
		next()
	}
});
app.use((req, res, next) => {
	try {
		if (utility.isRequestWebPage(req)) {
			// authorize
			if (req.user) {
				res.locals.username = req.user.username;
				res.locals.userGroup = req.user.userGroup;
				res.locals.userRights = req.user.userRights;
				var accessableMenu = defaultMenu;
				if (rolesModules) {
					if (res.locals.userRights) {
						accessableMenu = defaultMenu.filter(_menu => res.locals.userRights.includes(_menu.name))
						if (login)
							login.module.menu.forEach(_m => {
								accessableMenu.push({
									name: _m.name,
									group: _m.group,
									action: "/" + _m.name,
									selected: false,
									display: _m.display,
								})
							})
					} else {
						throw "no access right"
					}
				}
				//console.log(`/${req.url.split('/')[2]}`)
				if (!accessableMenu.some(_menu => req.url.startsWith(_menu.action)) && !nomarlPage.includes(req.url))
					throw "no access right"

				res.locals.menu = formatMenu(accessableMenu, req.url);
			} else {
				res.locals.menu = formatMenu(defaultMenu, req.url);
			}
		}
		next();
	} catch (ex) {
		res.status(500).send(ex);
	}
});
// if (backupRestore) {
// 	var backupRestoreModule = []
// 	modules.forEach((md) => {
// 		if (md.module.backupRestore)
// 			md.module.backupRestore.forEach(_module => {
// 				if (_module)
// 					backupRestoreModule.push(_module)
// 			})
// 	})
// 	backupRestore.module.initialize(configuration, backupRestoreModule);
// }
modules.forEach((md) => {
	if (md.module.menu)
		md.module.menu.forEach(_m => {
			defaultMenu.push({
				name: _m.name,
				group: _m.group,
				action: (md.moduleGroup ? "/" + md.moduleGroup : "") + "/" + _m.name,
				selected: false,
				display: _m.display,
			})
		})

	// if (md.name != process.env.LOGIN) {
	// if (md.name != process.env.BACKUPRESTORE) {
	md.module.initialize(configuration);
	// }
	if (md.moduleGroup && md.moduleGroup != md.name)
		app.use(`/${md.moduleGroup}/${md.name}`, md.module.router);
	else
		app.use(`/${md.name}`, md.module.router);
	console.log('router initialize ' + md.name);
// }
});


// nomarl router
app.get('/home', (req, res) => {

	res.render('home');
});
app.get('/', (req, res) => {
	res.redirect('home');
});


// catch 404 and forward to error handler

app.use((req, res, next) => {
	console.log('not found: ', req.url);
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use((err, req, res, next) => {
	try {
		if (req.app.get('env') === 'development') {
			console.log('error handler:', err);
		}

		res.locals.message = err.message;
		res.locals.error = err ;

		// console.log(req.app.get('env'));
		// render the error page
		if (!res.headersSent) {
			res.status(err.status || 500).end();
			res.render('error');
		}
	} catch (ex) {}

});



module.exports = app;